#!/usr/bin/env python3

import argparse
import lxml.etree
import os


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output", type=os.path.realpath)
    parser.add_argument("file", type=os.path.realpath, nargs="+")
    args = parser.parse_args()

    rootname = {
        "architecture.xml": "architectures",
        "endianness.xml": "endiannesses",
        "family.xml": "families",
        "feature.xml": "features",
    }.get(os.path.basename(args.output))

    schema = os.path.join(os.path.dirname(__file__), "schema.rng")
    with open(schema, "tr") as f:
        validator = lxml.etree.RelaxNG(lxml.etree.parse(f))
    parser = lxml.etree.XMLParser(remove_comments=True, remove_blank_text=True)
    root = lxml.etree.Element(rootname)

    names = set()
    for filename in args.file:
        with open(filename, "tr") as f:
            doc = lxml.etree.parse(f, parser=parser)
        if not validator.validate(doc):
            for line in validator.error_log:
                message = "{}:{}:{}: {}".format(
                    filename,
                    line.line,
                    line.column,
                    line.message
                )
                print(message, file=os.sys.stderr)
            exit(1)
        for node in doc.getroot():
            for name in node.xpath("./name/text()"):
                if name in names:
                    message = "{}:{}: Duplicated element '{}'".format(
                        filename,
                        node.sourceline,
                        name
                    )
                    print(message, file=os.sys.stderr)
                    exit(1)
                names.add(name)
            root.append(node)

    text = lxml.etree.tostring(
        root,
        encoding="UTF-8",
        xml_declaration=True,
        pretty_print=True)

    with open(args.output, "tw") as f:
        f.write(text.decode())


if __name__ == "__main__":
    main()
