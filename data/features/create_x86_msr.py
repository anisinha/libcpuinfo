#!/bin/env python3

import argparse
import os


def parse_args():
    parser = argparse.ArgumentParser(
        description="Create templates for x86 CPU feature XML files.")
    parser.add_argument(
        "index",
        help="register index")
    args = parser.parse_args()
    args.index = int(args.index, 0)
    return args


def filename(args):
    name = "x86_msr_{:08x}.xml".format(args.index)
    return os.path.join(os.path.dirname(__file__), name)


def write_template(f, args):
    f.write("<?xml version=\"1.0\"?>\n")
    f.write("<features>\n")
    for index in range(0, 64):
        mask = 1 << index
        eax = 0xffffffff & (mask >> 0)
        edx = 0xffffffff & (mask >> 32)
        f.write("    <!--\n")
        f.write("    <feature>\n")
        f.write("        <name></name>\n")
        f.write("        <aliases/>\n")
        f.write("        <description></description>\n")
        f.write("        <family>x86</family>\n")
        f.write("        <features/>\n")
        f.write("        <extra>\n")
        f.write("            <x86-msr>\n")
        f.write("                <index>0x{:08x}</index>\n".format(args.index))
        f.write("                <eax>0x{:08x}</eax>\n".format(eax))
        f.write("                <edx>0x{:08x}</edx>\n".format(edx))
        f.write("            </x86-msr>\n")
        f.write("        </extra>\n")
        f.write("    </feature>\n")
        f.write("    -->\n")
    f.write("</features>\n")


def main():
    args = parse_args()
    with open(filename(args), "tw") as f:
        write_template(f, args)


if __name__ == "__main__":
    main()
