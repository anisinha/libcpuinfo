# What is libcpuinfo?

Modern CPUs are becoming increasingly complex and differ in their supported
features, cache size, NUMA architecture, register layout, and many other
aspects. This detailed information is essential for many programs: Hypervisors,
virtualization and container orchestration applications, compilers, debuggers,
build systems and package managers.

Unfortunately, there is no central database that collects this information in
a machine readable way, but instead it is spread out over multiple sources
from different vendors, often in the form of thousands-of-pages pdf files
that are not easily parsed by scripts. As a result, this has led to code
duplication and differences in interpretations, naming and layout.

libcpuinfo aims to be this central, cross-vendor and cross-architecture, single
source of truth for any CPU related information that might be required by a
program, including convenient tooling for common use cases.

# Usage models

libcpuinfo is composed of three main parts: A "database" of xml files and
schemata containing the actual data; a library to access the xml files and
provide utility functions; and a command line interface using said library.

## Import data ahead of time

If you want to avoid any dependency on libcpuinfo in your project, it is
possible to update your project's data files from libcpuinfo and keep the
generated files with your project's source code. This requires manually
checking for changes or updates in libcpuinfo from time to time, but is the
least intrusive option.

## Add libcpuinfo as a build dependency

Generating your project's data file on compile time guarantees up-to-date CPU
data, but requires libcpuinfo to be installed when building your project. The
python bindings for libcpuinfo may come in handy and should be
usable regardless of used build system.

## Link against libcpuinfo

Linking your project against libcpuinfo allows your project to access updates
and changes to the libcpuinfo database without reinstallation or recompilation,
as libcpuinfo can be updated independently of your project. This requires
libcpuinfo to be installed on the target machine though.

# Using the command line interface

Use `cpuinfo --help` to display a short description of the available commands.
The command line interface lets you query cpu families, architectures,
features, and endianness, as well as cpuid and msr information (x86 only).

Colorization of the output is controlled with the `--color` switch, and the
desired output format is controlled with `--format`. libcpuinfo is currently
able to produce text, json and xml data.

Example:

```sh
$ cpuinfo feature # or: "cpuinfo --format text feature", this is the default
1gbpages
acpi
adx
...

$ cpuinfo --format json feature
[
    "1gbpages",
    "acpi",
    "adx",
    ...
]

$ cpuinfo --format xml feature
<list>
    <element>1gbpages</element>
    <element>acpi</element>
    <element>adx</element>
    ...
</list>
```

Example of querying a CPU feature, note the "hostsupport" field that indicates
support for this feature on the current CPU:

```sh
$ cpuinfo feature fpu
name:        fpu
description: Floating Point Unit On-Chip.
family:      x86
x86 cpuid:
    eax_in: 0x00000001
    eax:    0x00000000
    ebx:    0x00000000
    ecx:    0x00000000
    edx:    0x00000001
hostsupport: true
```

Example of calculating the set of common CPU features ("baseline") of two
different machines:

```sh
# Run on every host:
$ cpuinfo featureset host > featureset_$HOSTNAME.xml

# Calculate baseline:
$ cpuinfo featureset baseline featureset_*.xml > baseline.xml

# Result is an easy to use XML file:
$ head -n5 baseline.xml
<featureset>
  <family>x86</family>
  <features>
    <feature>1gbpages</feature>
    <feature>acpi</feature>

# Query presence of features in the baseline
$ cpuinfo featureset contains baseline.xml 1gbpages
true
$ cpuinfo featureset contains baseline.xml avx512bw
false
```
Example of querying cpuid information on a x86 CPU:

```sh
$ cpuinfo --format json cpuid 0x00
{
    "eax": 22,
    "ebx": 1970169159,
    "ecx": 1818588270,
    "edx": 1231384169
}
```

# Using the library

libcpuinfo comes with C headers which should allow easy integration into any
project and adaptation for other programming languages.

The following example (compile with `cc demo.c $(pkg-config --cflags --libs
libcpuinfo)`) demonstrates how to use the libcpuinfo library to create a
program that enumerates the CPU features supported on the current machine:

```c
#include <libcpuinfo/libcpuinfo.h>
#include <stdio.h>

int main() {
    if (cpuinfo_init() != 0)
        return 1;

    cpuinfo_family_t* host_family = cpuinfo_family_by_name("host", "");
    if (host_family == NULL)
        return 1;

    printf(
        "features supported on this host(%s):\n",
        cpuinfo_family_name_get(host_family, NULL));

    size_t count = cpuinfo_feature_count();
    for (size_t i = 0; i < count; ++i) {
        cpuinfo_feature_t* feature = cpuinfo_feature_by_index(i);
        printf(
            "%s: %s\n",
            cpuinfo_feature_name_get(feature, NULL),
            cpuinfo_feature_hostsupport_get(feature) ? "yes" : "no");

        cpuinfo_feature_free(feature);
    }

    cpuinfo_family_free(host_family);
    cpuinfo_quit();
    return 0;
}
```

# Using the python bindings

The following example achieves the same as the C example above, using the
provided python bindings:

```py
import pycpuinfo

host_family = pycpuinfo.Family.find("host", "")
print("features supported on this host({}):".format(host_family))

for feature in pycpuinfo.features():
    print("{}: {}".format(feature, feature.hostsupport()))
```

# Entity aliasing

To achieve better interoperability between software, and to provide an easier
upgrade path for projects to include libcpuinfo, every entity in libcpuinfo
can have multiple alias-names. These alias names can be generic or tied to a
specific domain and are displayed as `"domain:alias"`:

```sh
$ cpuinfo endianness little
name:        little
aliases:
    :LE
    :host
    :le
description: Stores the least-significant byte at the lowest address

$ cpuinfo endianness le
name:        little
aliases:
    :LE
    :host
    :le
description: Stores the least-significant byte at the lowest address
```

The canonical name of the endianness, that stores the least-significant byte at
the lowest address is `little`. libcpuinfo also recognizes the names `LE`,
`le`, and `host` for this endianness. The empty "domain" part of these aliases
indicates that those are generic aliases, not tied to a specific usage. The
`host` alias is applied dynamically: The CPU used to run this example is in
fact a little endian system.

Aliases that are domain specific, e.g. "someproject:little_endian" behave
exactly the same. In addition, calls to `const char* cpuinfo_*_name_get(object,
domain)` return the domain specific alias name instead of the canonical name:

```c
cpuinfo_endianness_t* little = ...;

// returns the canonical name "little"
cpuinfo_endianness_name_get(little, NULL);

// returns "little_endian"
cpuinfo_endianness_name_get(little, "someproject");

// also returns "little", as no domain specific alias for this domain is known
cpuinfo_endianness_name_get(little, "someotherproject");
```

# Building libcpuinfo

This is a meson project. It depends on libxml2 and optionally on gtest.

Build with
```
$ meson setup builddir
$ meson compile -C builddir
```

To disable tests (and the dependency on gtest), append `-Dwith_tests=disabled`
to the `meson setup builddir` line.

# License

This library is distributed under the terms of the Lesser GNU General Public
License, version 2.1 (or later). The command line interface and all other code
is distributed under the terms of GNU General Public License, version 2 (or
later). The full license text can be found in the files `copying_gpl.txt` and
`copying_lgpl.txt`.

Contributions are welcome.
