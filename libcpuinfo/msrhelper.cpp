#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <map>

void msr_read(std::map<uint32_t, uint64_t>& msrs, FILE* file, uint32_t index) {
    if (::fseek(file, index, 0) != 0) {
        return;
    }

    auto buffer = uint64_t {};
    if (::fread(&buffer, sizeof(buffer), 1, file) != 1) {
        return;
    }

    msrs[index] = buffer;
}

int main() {
    auto file = ::fopen("/dev/cpu/0/msr", "r+");
    if (file == nullptr) {
        ::fprintf(stderr, "Error: Cannot open msr file\n");
        return 1;
    }

    auto msrs = std::map<uint32_t, uint64_t> {};
    msr_read(msrs, file, 0x000000cfUL); // MSR_IA32_CORE_CAPS
    msr_read(msrs, file, 0x0000010aUL); // MSR_IA32_ARCH_CAPABILITIES
    msr_read(msrs, file, 0x00000345UL); // IA32_PERF_CAPABILITIES
    msr_read(msrs, file, 0x00000480UL); // IA32_VMX_BASIC
    msr_read(msrs, file, 0x00000485UL); // IA32_VMX_MISC
    msr_read(msrs, file, 0x0000048bUL); // IA32_VMX_PROCBASED_CTLS2
    msr_read(msrs, file, 0x0000048cUL); // IA32_VMX_EPT_VPID_CAP
    msr_read(msrs, file, 0x0000048dUL); // IA32_VMX_TRUE_PINBASED_CTLS
    msr_read(msrs, file, 0x0000048eUL); // IA32_VMX_TRUE_PROCBASED_CTLS
    msr_read(msrs, file, 0x0000048fUL); // IA32_VMX_TRUE_EXIT_CTLS
    msr_read(msrs, file, 0x00000490UL); // IA32_VMX_TRUE_ENTRY_CTLS
    msr_read(msrs, file, 0x00000491UL); // IA32_VMX_VMFUNC

    for (const auto& [key, value] : msrs) {
        ::printf("%u %lu\n", key, value);
    }
    return 0;
}
