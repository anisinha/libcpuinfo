// SPDX-License-Identifier: LGPL-2.1-or-later
// Copyright 2023 Tim Wiederhake

#include "x86.hpp"

#include "../feature.hpp"

#include <cstdio>
#include <cstring>
#include <map>
#include <mutex>

class Msr {
public:
    static constexpr auto m_filename = "/dev/cpu/0/msr";

    Msr() :
        m_lock { m_mutex } {

        if (m_file == nullptr) {
            m_file = ::fopen(m_filename, "r+");
            if (m_file == nullptr) {
                error();
            }
        }
    }

    Msr(const Msr&) = delete;

    Msr(Msr&&) = delete;

    Msr& operator=(const Msr&) = delete;

    Msr& operator=(Msr&&) = delete;

    ~Msr() = default;

    int read(uint32_t in_ecx, uint32_t* out_eax, uint32_t* out_edx) {
        if (m_file == nullptr) {
            return read_helper(in_ecx, out_eax, out_edx);
        }

        if (::fseek(m_file, in_ecx, 0) != 0) {
            return error();
        }

        char buffer[2 * sizeof(uint32_t)] {};
        if (::fread(buffer, sizeof(buffer), 1, m_file) != 1) {
            return error();
        }

        if (out_eax != nullptr) {
            std::memcpy(out_eax, buffer, sizeof(*out_eax));
        }

        if (out_edx != nullptr) {
            std::memcpy(out_edx, buffer + sizeof(*out_eax), sizeof(*out_edx));
        }

        return 0;
    }

    int write(uint32_t in_ecx, uint32_t in_eax, uint32_t in_edx) {
        if (m_file == nullptr) {
            return -1;
        }

        if (::fseek(m_file, in_ecx, 0) != 0) {
            return error();
        }

        char buffer[2 * sizeof(uint32_t)] {};
        std::memcpy(buffer, &in_eax, sizeof(in_eax));
        std::memcpy(buffer + sizeof(in_eax), &in_edx, sizeof(in_edx));

        if (::fwrite(buffer, sizeof(buffer), 1, m_file) != 1) {
            return error();
        }

        return 0;
    }

private:
    static std::map<uint32_t, uint64_t> m_from_helper;
    static std::mutex m_mutex;
    static FILE* m_file;
    std::lock_guard<std::mutex> m_lock;

    int error() {
        /* NOLINTNEXTLINE: strerror is not thread safe */
        auto message = std::string { std::strerror(errno) };
        if (message.empty()) {
            message = "I/O Error";
        }

        cpuinfo_error_set(m_filename + (": " + message));
        return -1;
    }

    int read_helper(uint32_t in_ecx, uint32_t* out_eax, uint32_t* out_edx) {
        if (m_from_helper.empty()) {
            auto index = uint32_t {};
            auto value = uint64_t {};
            auto stream = ::popen(CPUINFO_DATADIR "/msrhelper", "r");
            if (stream == nullptr) {
                return error();
            }
            while (fscanf(stream, "%u %lu", &index, &value) == 2) {
                m_from_helper[index] = value;
            }
            if (::pclose(stream) != 0) {
                m_from_helper.clear();
                return error();
            }
        }

        auto eax = uint32_t {};
        auto edx = uint32_t {};

        auto it = m_from_helper.find(in_ecx);
        if (it != m_from_helper.end()) {
            eax = 0xffffffffUL & (it->second >> 0UL);
            edx = 0xffffffffUL & (it->second >> 32UL);
        }

        if (out_eax != nullptr) {
            *out_eax = eax;
        }

        if (out_edx != nullptr) {
            *out_edx = edx;
        }

        return 0;
    }
};

std::map<uint32_t, uint64_t> Msr::m_from_helper {};
std::mutex Msr::m_mutex {};
FILE* Msr::m_file {};

template<typename T>
static void set_if(T* target, const T& source) {
    if (target == nullptr) {
        return;
    }

    *target = source;
}

struct cpuinfo_x86_cpuid_cache {
public:
    [[nodiscard]] bool is_leaf_valid(uint32_t eax, uint32_t ecx) const {
        auto ret = false;

        if (ecx == 0UL) {
            ret = true;
        } else if (eax == 0x04UL) {
            ret = !leaf_04_done;
        } else if (eax == 0x0BUL) {
            ret = !leaf_0B_done;
        } else if (eax == 0x0DUL) {
            ret = (ecx < 32) && ((leaf_0D_mask & (1ULL << ecx)) != 0);
        } else if (eax == 0x12UL) {
            ret = (ecx == 0x01UL) || !leaf_12_done;
        } else if (eax == 0x14UL) {
            ret = ecx == 0x01UL;
        }

        return ret;
    }

    void update(
            uint32_t eax_in,
            uint32_t ecx_in,
            uint32_t eax,
            uint32_t /* ebx */,
            uint32_t ecx,
            uint32_t /* edx */) {
        if (eax_in == 0x04UL) {
            leaf_04_done |= ((eax & 0x0FUL) == 0UL);
        } else if (eax_in == 0x0BU) {
            leaf_0B_done |= ((ecx & 0xFF00UL) == 0UL);
        } else if ((eax_in == 0x0DUL) && (ecx_in == 0UL)) {
            leaf_0D_mask |= eax;
        } else if ((eax_in == 0x0DUL) && (ecx_in == 1UL)) {
            leaf_0D_mask |= ecx;
        } else if (eax_in == 0x12UL) {
            leaf_12_done |= (ecx_in > 1 && ((eax & 0x0FUL) == 0UL));
        }
    }

private:
    bool leaf_04_done {};
    bool leaf_0B_done {};
    uint32_t leaf_0D_mask {};
    bool leaf_12_done {};
};

int cpuinfo_x86_cpuid(
        uint32_t in_eax,
        uint32_t in_ecx,
        uint32_t* out_eax,
        uint32_t* out_ebx,
        uint32_t* out_ecx,
        uint32_t* out_edx) {
    auto eax = uint32_t {};
    auto ebx = uint32_t {};
    auto ecx = uint32_t {};
    auto edx = uint32_t {};
    auto ret = -1;

#if defined(CPUINFO_FAMILY_IS_X86)
    asm volatile(
            "cpuid"
            : "=a"(eax), "=b"(ebx), "=c"(ecx), "=d"(edx)
            : "a"(in_eax), "c"(in_ecx));
    ret = 0;
#else
    cpuinfo_error_set("function not supported on this architecture");
#endif

    set_if(out_eax, eax);
    set_if(out_ebx, ebx);
    set_if(out_ecx, ecx);
    set_if(out_edx, edx);
    return ret;
}

int cpuinfo_x86_cpuid_foreach(cpuinfo_x86_cpuid_callback* cb, void* userdata) {
    auto ret = int {};

    auto level = uint32_t {};
    ret = cpuinfo_x86_cpuid(
            CPUINFO_X86_CPUID_LEAF_BASIC,
            0UL,
            &level,
            nullptr,
            nullptr,
            nullptr);
    if (ret < 0) {
        return -1;
    }

    auto xlevel = uint32_t {};
    ret = cpuinfo_x86_cpuid(
            CPUINFO_X86_CPUID_LEAF_EXTENDED,
            0UL,
            &xlevel,
            nullptr,
            nullptr,
            nullptr);
    if (ret < 0) {
        return -1;
    }

    auto cache = cpuinfo_x86_cpuid_cache {};

    for (uint32_t eax_in = 0UL; eax_in <= xlevel; ++eax_in) {
        if ((eax_in > level) && (eax_in < CPUINFO_X86_CPUID_LEAF_EXTENDED)) {
            eax_in = CPUINFO_X86_CPUID_LEAF_EXTENDED;
        }

        for (uint32_t ecx_in = 0UL; ecx_in < 0xFFUL; ++ecx_in) {
            if (!cache.is_leaf_valid(eax_in, ecx_in)) {
                continue;
            }

            auto eax = uint32_t {};
            auto ebx = uint32_t {};
            auto ecx = uint32_t {};
            auto edx = uint32_t {};

            ret = cpuinfo_x86_cpuid(eax_in, ecx_in, &eax, &ebx, &ecx, &edx);
            if (ret != 0) {
                return -1;
            }

            ret = cb(eax_in, ecx_in, eax, ebx, ecx, edx, userdata);
            if (ret != 0) {
                return ret;
            }

            cache.update(eax_in, ecx_in, eax, ebx, ecx, edx);
        }
    }

    return 0;
}

int cpuinfo_x86_msr_read(
        uint32_t in_ecx,
        uint32_t* out_eax,
        uint32_t* out_edx) {
    return Msr().read(in_ecx, out_eax, out_edx);
}

int cpuinfo_x86_msr_write(uint32_t in_ecx, uint32_t in_eax, uint32_t in_edx) {
    return Msr().write(in_ecx, in_eax, in_edx);
}

int cpuinfo_feature_x86_extra_cpuid_get(
        cpuinfo_feature_t* obj,
        uint32_t* eax_in,
        uint32_t* ecx_in,
        uint32_t* eax,
        uint32_t* ebx,
        uint32_t* ecx,
        uint32_t* edx) {
    if (obj == nullptr) {
        cpuinfo_error_set_func_null(__func__);
        return -1;
    }

    auto ptr = std::get_if<cpuinfo_feature_extra_x86_cpuid_t>(&obj->extra());
    if (ptr == nullptr) {
        return 1;
    }

    set_if(eax_in, ptr->eax_in);
    set_if(ecx_in, ptr->ecx_in);
    set_if(eax, ptr->eax);
    set_if(ebx, ptr->ebx);
    set_if(ecx, ptr->ecx);
    set_if(edx, ptr->edx);
    return 0;
}

int cpuinfo_feature_x86_extra_msr_get(
        cpuinfo_feature_t* obj,
        uint32_t* index,
        uint32_t* eax,
        uint32_t* edx) {
    if (obj == nullptr) {
        cpuinfo_error_set_func_null(__func__);
        return -1;
    }

    auto ptr = std::get_if<cpuinfo_feature_extra_x86_msr_t>(&obj->extra());
    if (ptr == nullptr) {
        return 1;
    }

    set_if(index, ptr->index);
    set_if(eax, ptr->eax);
    set_if(edx, ptr->edx);
    return 0;
}
