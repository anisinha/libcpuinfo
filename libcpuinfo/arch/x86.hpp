// SPDX-License-Identifier: LGPL-2.1-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include "../libcpuinfo.hpp"

#include <libcpuinfo/arch/x86.h>
