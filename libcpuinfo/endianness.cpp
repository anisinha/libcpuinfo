// SPDX-License-Identifier: LGPL-2.1-or-later
// Copyright 2023 Tim Wiederhake

#include "endianness.hpp"

#include "data.hpp"

std::vector<std::unique_ptr<cpuinfo_endianness_t>> cpuinfo_endianness_t::all {};

size_t cpuinfo_endianness_count(void) {
    return cpuinfo_endianness_t::all.size();
}

cpuinfo_endianness_t* cpuinfo_endianness_by_index(size_t index) {
    if (index >= cpuinfo_endianness_t::all.size()) {
        return cpuinfo_error_set_index_oob(__func__);
    }

    return cpuinfo_endianness_t::all[index].get();
}

cpuinfo_endianness_t* cpuinfo_endianness_by_name(
        const char* name,
        const char* domain) {
    return alias_find(__func__, cpuinfo_endianness_t::all, name, domain);
}

void cpuinfo_endianness_free(cpuinfo_endianness_t* /* obj */) {
    /* all endiannesses are internally allocated and need not be free'd. */
}

const char* cpuinfo_endianness_name_get(
        cpuinfo_endianness_t* obj,
        const char* domain) {
    return alias_get(__func__, obj, domain);
}

size_t cpuinfo_endianness_alias_count(cpuinfo_endianness_t* obj) {
    return alias_count(__func__, obj);
}

cpuinfo_alias_t* cpuinfo_endianness_alias_get(
        cpuinfo_endianness_t* obj,
        size_t index) {
    return alias_index(__func__, obj, index);
}

const char* cpuinfo_endianness_description_get(cpuinfo_endianness_t* obj) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    return obj->description().c_str();
}

char* cpuinfo_endianness_xml_get(cpuinfo_endianness_t* obj) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    auto string = cpuinfo_data_save([=](xmlNode* node) {
        return cpuinfo_data_encode(node, *obj);
    });

    return cpuinfo_data_strdup(string);
}

void cpuinfo_data_decode(xmlNode* xml, cpuinfo_endianness_t& value) {
    for (auto node : XmlRange { xml }) {
        auto name = cpuinfo_data_node_name(node);

        if (name == "name") {
            cpuinfo_data_decode(node, value.name());
        } else if (name == "aliases") {
            cpuinfo_data_decode(node, value.aliases());
        } else if (name == "description") {
            cpuinfo_data_decode(node, value.description());
        }
    }
}

xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const cpuinfo_endianness_t& value,
        const std::string& name) {
    auto node = cpuinfo_data_create(parent, name);
    cpuinfo_data_encode(node, value.name(), "name");
    cpuinfo_data_encode(node, value.aliases(), "aliases", "alias");
    cpuinfo_data_encode(node, value.description(), "description");
    return node;
}

int cpuinfo_endianness_equals(
        cpuinfo_endianness_t* lhs,
        cpuinfo_endianness_t* rhs) {
    return lhs == rhs ? CPUINFO_TRUE : CPUINFO_FALSE;
}
