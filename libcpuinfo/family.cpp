// SPDX-License-Identifier: LGPL-2.1-or-later
// Copyright 2023 Tim Wiederhake

#include "family.hpp"

#include "data.hpp"

std::vector<std::unique_ptr<cpuinfo_family_t>> cpuinfo_family_t::all {};

size_t cpuinfo_family_count(void) {
    return cpuinfo_family_t::all.size();
}

cpuinfo_family_t* cpuinfo_family_by_index(size_t index) {
    if (index >= cpuinfo_family_t::all.size()) {
        return cpuinfo_error_set_index_oob(__func__);
    }

    return cpuinfo_family_t::all[index].get();
}

cpuinfo_family_t* cpuinfo_family_by_name(const char* name, const char* domain) {
    return alias_find(__func__, cpuinfo_family_t::all, name, domain);
}

void cpuinfo_family_free(cpuinfo_family_t* /* obj */) {
    /* all families are internally allocated and need not be free'd. */
}

const char* cpuinfo_family_name_get(cpuinfo_family_t* obj, const char* domain) {
    return alias_get(__func__, obj, domain);
}

size_t cpuinfo_family_alias_count(cpuinfo_family_t* obj) {
    return alias_count(__func__, obj);
}

cpuinfo_alias_t* cpuinfo_family_alias_get(cpuinfo_family_t* obj, size_t index) {
    return alias_index(__func__, obj, index);
}

const char* cpuinfo_family_description_get(cpuinfo_family_t* obj) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    return obj->description().c_str();
}

char* cpuinfo_family_xml_get(cpuinfo_family_t* obj) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    auto string = cpuinfo_data_save([=](xmlNode* node) {
        return cpuinfo_data_encode(node, *obj);
    });

    return cpuinfo_data_strdup(string);
}

void cpuinfo_data_decode(xmlNode* xml, cpuinfo_family_t& value) {
    for (auto node : XmlRange { xml }) {
        auto name = cpuinfo_data_node_name(node);

        if (name == "name") {
            cpuinfo_data_decode(node, value.name());
        } else if (name == "aliases") {
            cpuinfo_data_decode(node, value.aliases());
        } else if (name == "description") {
            cpuinfo_data_decode(node, value.description());
        }
    }
}

xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const cpuinfo_family_t& value,
        const std::string& name) {
    auto node = cpuinfo_data_create(parent, name);
    cpuinfo_data_encode(node, value.name(), "name");
    cpuinfo_data_encode(node, value.aliases(), "aliases", "alias");
    cpuinfo_data_encode(node, value.description(), "description");
    return node;
}

int cpuinfo_family_equals(cpuinfo_family_t* lhs, cpuinfo_family_t* rhs) {
    return lhs == rhs ? CPUINFO_TRUE : CPUINFO_FALSE;
}
