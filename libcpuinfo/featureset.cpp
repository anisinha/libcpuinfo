// SPDX-License-Identifier: LGPL-2.1-or-later
// Copyright 2023 Tim Wiederhake

#include "featureset.hpp"

#include "data.hpp"

#include <map>

static std::nullptr_t cpuinfo_error_featureset_incompatible_family(
        const std::string_view& func,
        cpuinfo_family_t* expected,
        cpuinfo_family_t* actual) {
    auto message = std::string {};
    message += func;
    message += " called with incompatible families \"";
    message += expected->name();
    message += "\" and \"";
    message += actual->name();
    message += "\"";

    cpuinfo_error_set(message);
    return nullptr;
}

cpuinfo_featureset_t* cpuinfo_featureset_new(cpuinfo_family_t* family) {
    if (family == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    auto featureset = new cpuinfo_featureset_t {};
    featureset->family() = family;
    return featureset;
}

cpuinfo_featureset_t* cpuinfo_featureset_all(cpuinfo_family_t* family) {
    if (family == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    auto featureset = new cpuinfo_featureset_t {};
    featureset->family() = family;
    for (auto& feature : cpuinfo_feature_t::all) {
        if (feature->family() != family) {
            continue;
        }

        cpuinfo_featureset_feature_add(featureset, feature.get());
    }

    return featureset;
}

cpuinfo_featureset_t* cpuinfo_featureset_host(void) {
    auto featureset = std::make_unique<cpuinfo_featureset_t>();

    featureset->family() = cpuinfo_family_by_name("host", "");
    if (featureset->family() == nullptr) {
        return nullptr;
    }

    for (auto& feature : cpuinfo_feature_t::all) {
        if (feature->family() != featureset->family()) {
            continue;
        }

        auto ret = cpuinfo_feature_hostsupport_get(feature.get());
        if (ret < 0) {
            return nullptr;
        }

        if (ret == 0) {
            continue;
        }

        cpuinfo_featureset_feature_add(featureset.get(), feature.get());
    }

    return featureset.release();
}

cpuinfo_featureset_t* cpuinfo_featureset_clone(cpuinfo_featureset_t* obj) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    auto featureset = new cpuinfo_featureset_t {};
    featureset->family() = obj->family();
    for (auto& feature : obj->features()) {
        featureset->features().insert(feature);
    }

    return featureset;
}

void cpuinfo_featureset_free(cpuinfo_featureset_t* obj) {
    delete (obj);
}

int cpuinfo_featureset_equals(
        cpuinfo_featureset_t* lhs,
        cpuinfo_featureset_t* rhs) {
    if (lhs == nullptr || rhs == nullptr) {
        cpuinfo_error_set_func_null(__func__);
        return -1;
    }

    if (lhs->family() != rhs->family()) {
        return CPUINFO_FALSE;
    }

    if (lhs->features() != rhs->features()) {
        return CPUINFO_FALSE;
    }

    return CPUINFO_TRUE;
}

cpuinfo_family_t* cpuinfo_featureset_family_get(cpuinfo_featureset_t* obj) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    return obj->family();
}

size_t cpuinfo_featureset_feature_count(cpuinfo_featureset_t* obj) {
    if (obj == nullptr) {
        cpuinfo_error_set_func_null(__func__);
        return 0;
    }

    return obj->features().size();
}

cpuinfo_feature_t* cpuinfo_featureset_feature_get(
        cpuinfo_featureset_t* obj,
        size_t index) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    if (index >= obj->features().size()) {
        return cpuinfo_error_set_index_oob(__func__);
    }

    auto it = obj->features().begin();
    std::advance(it, index);
    return *it;
}

int cpuinfo_featureset_feature_add(
        cpuinfo_featureset_t* obj,
        cpuinfo_feature_t* feature) {
    if (obj == nullptr || feature == nullptr) {
        cpuinfo_error_set_func_null(__func__);
        return -1;
    }

    if (obj->family() != feature->family()) {
        cpuinfo_error_featureset_incompatible_family(
                __func__,
                obj->family(),
                feature->family());
        return -1;
    }

    if (feature->features().empty()) {
        auto [_, inserted] = obj->features().insert(feature);
        return inserted ? CPUINFO_TRUE : CPUINFO_FALSE;
    }

    auto ret = CPUINFO_FALSE;

    for (auto& features : feature->features()) {
        auto add = cpuinfo_featureset_feature_add(obj, features);
        if (add < 0) {
            return add;
        }
        if (add == CPUINFO_TRUE) {
            ret = CPUINFO_TRUE;
        }
    }

    return ret;
}

int cpuinfo_featureset_feature_remove(
        cpuinfo_featureset_t* obj,
        cpuinfo_feature_t* feature) {
    if (obj == nullptr || feature == nullptr) {
        cpuinfo_error_set_func_null(__func__);
        return 0;
    }

    /* no need to check for compatible family */

    auto ret = CPUINFO_FALSE;

    for (auto& features : feature->features()) {
        auto remove = cpuinfo_featureset_feature_remove(obj, features);
        if (remove < 0) {
            return remove;
        }
        if (remove == CPUINFO_TRUE) {
            ret = CPUINFO_TRUE;
        }
    }

    if (obj->features().erase(feature) > 0) {
        ret = CPUINFO_TRUE;
    }

    return ret;
}

void cpuinfo_featureset_feature_clear(cpuinfo_featureset_t* obj) {
    if (obj == nullptr) {
        cpuinfo_error_set_func_null(__func__);
        return;
    }

    obj->features().clear();
}

int cpuinfo_featureset_feature_contains(
        cpuinfo_featureset_t* obj,
        cpuinfo_feature_t* feature) {
    if (obj == nullptr || feature == nullptr) {
        cpuinfo_error_set_func_null(__func__);
        return -1;
    }

    if (feature->features().empty()) {
        if (obj->features().find(feature) == obj->features().end()) {
            return CPUINFO_FALSE;
        }

        return CPUINFO_TRUE;
    }

    for (auto& features : feature->features()) {
        auto contained = cpuinfo_featureset_feature_contains(obj, features);
        if (contained == CPUINFO_FALSE) {
            return CPUINFO_FALSE;
        }
    }

    return CPUINFO_TRUE;
}

cpuinfo_featureset_t* cpuinfo_featureset_featureset_union(
        cpuinfo_featureset_t* lhs,
        cpuinfo_featureset_t* rhs) {
    if (lhs == nullptr || rhs == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    if (lhs->family() != rhs->family()) {
        return cpuinfo_error_featureset_incompatible_family(
                __func__,
                lhs->family(),
                rhs->family());
    }

    auto featureset = new cpuinfo_featureset_t {};
    featureset->family() = lhs->family();
    featureset->features().insert(
            lhs->features().begin(),
            lhs->features().end());
    featureset->features().insert(
            rhs->features().begin(),
            rhs->features().end());
    return featureset;
}

cpuinfo_featureset_t* cpuinfo_featureset_featureset_intersect(
        cpuinfo_featureset_t* lhs,
        cpuinfo_featureset_t* rhs) {
    if (lhs == nullptr || rhs == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    if (lhs->family() != rhs->family()) {
        return cpuinfo_error_featureset_incompatible_family(
                __func__,
                lhs->family(),
                rhs->family());
    }

    auto featureset = new cpuinfo_featureset_t {};
    featureset->family() = lhs->family();
    for (auto& feature : lhs->features()) {
        if (rhs->features().find(feature) == rhs->features().end()) {
            continue;
        }

        featureset->features().insert(feature);
    }

    return featureset;
}

cpuinfo_featureset_t* cpuinfo_featureset_featureset_subtract(
        cpuinfo_featureset_t* lhs,
        cpuinfo_featureset_t* rhs) {
    if (lhs == nullptr || rhs == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    if (lhs->family() != rhs->family()) {
        return cpuinfo_error_featureset_incompatible_family(
                __func__,
                lhs->family(),
                rhs->family());
    }

    auto featureset = new cpuinfo_featureset_t {};
    featureset->family() = lhs->family();
    for (auto& feature : lhs->features()) {
        if (rhs->features().find(feature) != rhs->features().end()) {
            continue;
        }

        featureset->features().insert(feature);
    }

    return featureset;
}

cpuinfo_featureset_t* cpuinfo_featureset_xml_set(const char* filename) {
    if (filename == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    auto featureset = std::make_unique<cpuinfo_featureset_t>();

    try {
        cpuinfo_data_load(filename, [&](xmlNode* node) {
            cpuinfo_data_decode(node, *(featureset.get()));
        });
    } catch (const std::exception& e) {
        cpuinfo_error_set(e.what());
        return nullptr;
    }

    return featureset.release();
}

char* cpuinfo_featureset_xml_get(cpuinfo_featureset_t* obj) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    auto string = cpuinfo_data_save([=](xmlNode* node) {
        return cpuinfo_data_encode(node, *obj);
    });

    return cpuinfo_data_strdup(string);
}

void cpuinfo_data_decode(xmlNode* xml, cpuinfo_featureset_t& value) {
    for (auto node : XmlRange { xml }) {
        auto name = cpuinfo_data_node_name(node);

        if (name == "family") {
            cpuinfo_data_lookup(node, value.family());
        } else if (name == "features") {
            auto features = std::vector<cpuinfo_feature_t*> {};
            cpuinfo_data_lookup(node, features);
            for (const auto& feature : features) {
                if (cpuinfo_featureset_feature_add(&value, feature) < 0) {
                    throw std::runtime_error { cpuinfo_error_get() };
                }
            }
        }
    }
}

xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const cpuinfo_featureset_t& value,
        const std::string& name) {
    /* features to encode and whether it is already covered */
    auto working_set = std::map<cpuinfo_feature_t*, bool> {};
    for (auto& feature : value.features()) {
        working_set[feature] = false;
    }

    /* potential features to use to encode this feature set*/
    auto candidates = std::vector<cpuinfo_feature_t*> {};
    for (auto& candidate : cpuinfo_feature_t::all) {
        if (value.family() != candidate->family()) {
            continue;
        }

        candidates.push_back(candidate.get());
    }

    /* sort by size to catch big sets before encoding individual features */
    auto sort_by_size = [](cpuinfo_feature_t* lhs, cpuinfo_feature_t* rhs) {
        return lhs->features().size() > rhs->features().size();
    };
    std::sort(candidates.begin(), candidates.end(), sort_by_size);

    /* progressively collect features */
    auto features = std::vector<cpuinfo_feature_t*> {};
    for (auto& candidate : candidates) {
        auto it = working_set.find(candidate);

        /* skip if feature not in working set or already covered*/
        auto skip = (it == working_set.end()) || it->second;

        for (auto& feature : candidate->features()) {
            auto it = working_set.find(feature);

            /* skip if any child is not in working set */
            if (it == working_set.end()) {
                skip = true;
                break;
            }

            /* don't skip if candidate reduces working set */
            skip &= it->second;
        }

        if (skip) {
            continue;
        }

        /* add feature to output list */
        features.push_back(candidate);

        /* set feature + children to done */
        std::vector<cpuinfo_feature_t*> setdone { candidate };
        while (!setdone.empty()) {
            auto f = setdone.back();
            setdone.pop_back();

            working_set[f] = true;

            setdone.insert(
                    setdone.end(),
                    f->features().begin(),
                    f->features().end());
        }
    }

    /* sort by name to get a stable output */
    auto sort_by_name = [](cpuinfo_feature_t* lhs, cpuinfo_feature_t* rhs) {
        return lhs->name() < rhs->name();
    };
    std::sort(features.begin(), features.end(), sort_by_name);

    auto node = cpuinfo_data_create(parent, name);
    cpuinfo_data_encode(node, value.family(), "family");
    cpuinfo_data_encode(node, features, "features", "feature");
    return node;
}
