// SPDX-License-Identifier: LGPL-2.1-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include <functional>
#include <iterator>
#include <memory>
#include <vector>

typedef struct _xmlNode xmlNode;

class XmlRange {
public:
    class Iterator {
    public:
        using iterator_category = std::forward_iterator_tag;
        using difference_type = std::ptrdiff_t;
        using value_type = xmlNode*;

        explicit Iterator(xmlNode* ptr) :
            m_ptr { ptr } {
        }

        xmlNode* operator*() const {
            return m_ptr;
        }

        Iterator& operator++();

        Iterator operator++(int);

        friend bool operator==(const Iterator& a, const Iterator& b) {
            return a.m_ptr == b.m_ptr;
        };

        friend bool operator!=(const Iterator& a, const Iterator& b) {
            return a.m_ptr != b.m_ptr;
        };

    private:
        xmlNode* m_ptr;
    };

    explicit XmlRange(xmlNode* xml) :
        m_xml { xml } {
    }

    Iterator begin();

    Iterator end();

private:
    xmlNode* m_xml;
};

/**
 * Return an xml node's name.
 *
 * @param xml xml node.
 * @return the node's name.
 */
std::string cpuinfo_data_node_name(xmlNode* xml);

/**
 * Return an xml node's content as `std::string`.
 *
 * @param xml xml node.
 * @param value storage for content.
 */
void cpuinfo_data_decode(xmlNode* xml, std::string& value);

/**
 * Return an xml node's content as `int`.
 *
 * @param xml xml node.
 * @param value storage for content.
 */
void cpuinfo_data_decode(xmlNode* xml, int& value);

/**
 * Return an xml node's content as `std::vector<>`.
 *
 * @param xml xml node.
 * @param value storage for content.
 */
template<typename T>
void cpuinfo_data_decode(xmlNode* xml, std::vector<T>& value) {
    for (auto node : XmlRange { xml }) {
        auto element = T {};
        cpuinfo_data_decode(node, element);
        value.push_back(std::move(element));
    }
}

/**
 * Return an xml node's content as `std::vector<>`. Special case for creating
 * the object pools.
 *
 * @param xml xml node.
 * @param value storage for content.
 */
template<typename T>
void cpuinfo_data_decode(xmlNode* xml, std::vector<std::unique_ptr<T>>& value) {
    for (auto node : XmlRange { xml }) {
        auto element = std::make_unique<T>();
        cpuinfo_data_decode(node, *element);
        value.push_back(std::move(element));
    }
}

/**
 * Look up an xml node's content as single object from its respective object
 * pool.
 *
 * @param xml xml node.
 * @param value storage for content.
 */
template<typename T>
void cpuinfo_data_lookup(xmlNode* xml, T*& value) {
    auto string = std::string {};
    cpuinfo_data_decode(xml, string);

    for (auto& element : value->all) {
        if (element->name() == string) {
            value = element.get();
            return;
        }
    }

    throw std::invalid_argument {
        "unknown reference: \"" + string + "\""
    };
}

/**
 * Look up an xml node's content as vector of objects from its respective
 * object pool.
 *
 * @param xml xml node.
 * @param value storage for content.
 */
template<typename T>
void cpuinfo_data_lookup(xmlNode* xml, std::vector<T*>& values) {
    for (auto node : XmlRange { xml }) {
        auto value = static_cast<T*>(nullptr);
        cpuinfo_data_lookup(node, value);
        values.push_back(value);
    }
}

/**
 * Create a new xml node below a given parent.
 *
 * @param parent parent xml node.
 * @param name new xml node's name.
 * @return new xml node.
 */
xmlNode* cpuinfo_data_create(xmlNode* parent, const std::string& name);

/**
 * Create a new xml node below a given parent with content.
 *
 * @param parent parent xml node.
 * @param value content of new node.
 * @param name new xml node's name.
 * @return new xml node.
 */
xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const std::string& value,
        const std::string& name);

/**
 * Create a new xml node below a given parent with content.
 *
 * @param parent parent xml node.
 * @param value content of new node.
 * @param name new xml node's name.
 * @return new xml node.
 */
xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const int& value,
        const std::string& name);

/**
 * Create a new xml node below a given parent with reference to content.
 *
 * @param parent parent xml node.
 * @param value content of new node.
 * @param name new xml node's name.
 * @return new xml node.
 */
template<typename T>
xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const T* value,
        const std::string& name) {
    return cpuinfo_data_encode(parent, value->name(), name);
}

/**
 * Create a new xml node below a given parent with reference to content.
 *
 * @param parent parent xml node.
 * @param value content of new node.
 * @param name new xml node's name.
 * @return new xml node.
 */
template<typename T>
xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const std::vector<T>& values,
        const std::string& parent_name,
        const std::string& name) {
    auto node = cpuinfo_data_create(parent, parent_name);
    for (const auto& value : values) {
        cpuinfo_data_encode(node, value, name);
    }
    return node;
}

/**
 * Load xml data from file.
 *
 * @param filename file name.
 * @param func functor to call with file content.
 */
void cpuinfo_data_load(
        const std::string& filename,
        std::function<void(xmlNode*)> func);

/**
 * Store xml data to string.
 *
 * @param func functor to generate data.
 * @return xml representation as string.
 */
std::string cpuinfo_data_save(std::function<xmlNode*(xmlNode*)> func);

/**
 * Duplicate a string.
 *
 * The returned string must be released by `std::free()`.
 *
 * @param buffer string to duplicate.
 * @return new string with same content.
 */
char* cpuinfo_data_strdup(const std::string& buffer);
