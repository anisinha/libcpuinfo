// SPDX-License-Identifier: LGPL-2.1-or-later
// Copyright 2023 Tim Wiederhake

#include "featureset.hpp"

#include <gtest/gtest.h>

using ufam = std::unique_ptr<cpuinfo_family_t>;
using ufeat = std::unique_ptr<cpuinfo_feature_t>;
using uset = std::unique_ptr<cpuinfo_featureset_t>;

static auto family_a = new cpuinfo_family_t {};
static auto family_b = new cpuinfo_family_t {};

static auto a = new cpuinfo_feature_t {};
static auto b = new cpuinfo_feature_t {};
static auto c = new cpuinfo_feature_t {};
static auto d = new cpuinfo_feature_t {};
static auto x = new cpuinfo_feature_t {};

TEST(Featureset, New) {
    auto f = uset { cpuinfo_featureset_new(family_a) };
    EXPECT_EQ(f->family(), family_a);
    EXPECT_EQ(f->features().size(), 0);
}

TEST(Featureset, All) {
    auto f = uset { cpuinfo_featureset_all(family_a) };
    EXPECT_EQ(f->family(), family_a);
    EXPECT_EQ(f->features().size(), 3);
}

TEST(Featureset, Clone) {
    auto f1 = uset { cpuinfo_featureset_all(family_a) };
    auto f2 = uset { cpuinfo_featureset_clone(f1.get()) };
    EXPECT_EQ(f1->family(), f2->family());
    EXPECT_EQ(f1->features(), f2->features());
}

TEST(Featureset, Equals) {
    auto f1 = uset { cpuinfo_featureset_new(family_a) };
    auto f2 = uset { cpuinfo_featureset_new(family_a) };
    EXPECT_EQ(cpuinfo_featureset_equals(f1.get(), f2.get()), CPUINFO_TRUE);

    f2->features().insert(a);
    EXPECT_EQ(cpuinfo_featureset_equals(f1.get(), f2.get()), CPUINFO_FALSE);

    auto f3 = uset { cpuinfo_featureset_new(family_b) };
    EXPECT_EQ(cpuinfo_featureset_equals(f1.get(), f3.get()), CPUINFO_FALSE);
}

TEST(Featureset, Add) {
    auto f1 = uset { cpuinfo_featureset_new(family_a) };
    EXPECT_EQ(cpuinfo_featureset_feature_add(f1.get(), a), CPUINFO_TRUE);
    EXPECT_EQ(cpuinfo_featureset_feature_add(f1.get(), b), CPUINFO_TRUE);
    EXPECT_EQ(cpuinfo_featureset_feature_add(f1.get(), c), CPUINFO_TRUE);
    EXPECT_EQ(cpuinfo_featureset_feature_add(f1.get(), c), CPUINFO_FALSE);

    auto f2 = uset { cpuinfo_featureset_new(family_a) };
    EXPECT_EQ(cpuinfo_featureset_feature_add(f2.get(), a), CPUINFO_TRUE);
    EXPECT_EQ(cpuinfo_featureset_feature_add(f2.get(), d), CPUINFO_TRUE);
    EXPECT_EQ(cpuinfo_featureset_feature_add(f2.get(), d), CPUINFO_FALSE);
    EXPECT_EQ(f1->features(), f2->features());

    EXPECT_LT(cpuinfo_featureset_feature_add(f2.get(), x), 0);
}

TEST(Featureset, Remove) {
    auto f1 = uset { cpuinfo_featureset_all(family_a) };
    EXPECT_EQ(cpuinfo_featureset_feature_remove(f1.get(), a), CPUINFO_TRUE);
    EXPECT_EQ(cpuinfo_featureset_feature_remove(f1.get(), b), CPUINFO_TRUE);
    EXPECT_EQ(cpuinfo_featureset_feature_remove(f1.get(), c), CPUINFO_TRUE);
    EXPECT_EQ(cpuinfo_featureset_feature_remove(f1.get(), c), CPUINFO_FALSE);
    EXPECT_TRUE(f1->features().empty());

    auto f2 = uset { cpuinfo_featureset_all(family_a) };
    EXPECT_EQ(cpuinfo_featureset_feature_remove(f2.get(), a), CPUINFO_TRUE);
    EXPECT_EQ(cpuinfo_featureset_feature_remove(f2.get(), d), CPUINFO_TRUE);
    EXPECT_EQ(cpuinfo_featureset_feature_remove(f2.get(), d), CPUINFO_FALSE);
    EXPECT_TRUE(f2->features().empty());

    EXPECT_EQ(cpuinfo_featureset_feature_remove(f2.get(), x), CPUINFO_FALSE);

    auto f3 = uset { cpuinfo_featureset_new(family_a) };
    EXPECT_TRUE(cpuinfo_featureset_feature_add(f3.get(), d));
    EXPECT_EQ(cpuinfo_featureset_feature_remove(f3.get(), c), CPUINFO_TRUE);

    auto f4 = uset { cpuinfo_featureset_new(family_a) };
    EXPECT_TRUE(cpuinfo_featureset_feature_add(f4.get(), a));
    EXPECT_TRUE(cpuinfo_featureset_feature_add(f4.get(), b));
    EXPECT_EQ(f3->features(), f4->features());
}

TEST(Featureset, Clear) {
    auto f1 = uset { cpuinfo_featureset_all(family_a) };
    cpuinfo_featureset_feature_clear(f1.get());
    EXPECT_TRUE(f1->features().empty());
}

TEST(Featureset, Contains) {
    auto f1 = uset { cpuinfo_featureset_all(family_a) };
    EXPECT_EQ(cpuinfo_featureset_feature_contains(f1.get(), a), CPUINFO_TRUE);
    EXPECT_EQ(cpuinfo_featureset_feature_contains(f1.get(), d), CPUINFO_TRUE);

    cpuinfo_featureset_feature_remove(f1.get(), a);
    EXPECT_EQ(cpuinfo_featureset_feature_contains(f1.get(), a), CPUINFO_FALSE);
    EXPECT_EQ(cpuinfo_featureset_feature_contains(f1.get(), d), CPUINFO_FALSE);

    EXPECT_EQ(cpuinfo_featureset_feature_contains(f1.get(), x), CPUINFO_FALSE);
}

TEST(Featureset, Union) {
    auto f1 = uset { cpuinfo_featureset_new(family_a) };
    f1->features().insert({ a, b });

    auto f2 = uset { cpuinfo_featureset_new(family_a) };
    f2->features().insert(c);

    auto f3 = uset { cpuinfo_featureset_featureset_union(f1.get(), f2.get()) };
    auto f4 = uset { cpuinfo_featureset_all(family_a) };
    EXPECT_EQ(f3->features(), f4->features());

    auto f5 = uset { cpuinfo_featureset_new(family_b) };
    auto f6 = uset { cpuinfo_featureset_featureset_union(f1.get(), f5.get()) };
    EXPECT_FALSE(static_cast<bool>(f6));
}

TEST(Featureset, Intersect) {
    auto f1 = uset { cpuinfo_featureset_new(family_a) };
    f1->features().insert({ a, b });

    auto f2 = uset { cpuinfo_featureset_new(family_a) };
    f2->features().insert({ b, c });

    auto f3 = uset {
        cpuinfo_featureset_featureset_intersect(f1.get(), f2.get())
    };
    auto f4 = uset { cpuinfo_featureset_new(family_a) };
    f4->features().insert(b);
    EXPECT_EQ(cpuinfo_featureset_equals(f3.get(), f4.get()), CPUINFO_TRUE);

    auto f5 = uset { cpuinfo_featureset_new(family_a) };
    auto f6 = uset {
        cpuinfo_featureset_featureset_intersect(f1.get(), f5.get())
    };
    EXPECT_EQ(cpuinfo_featureset_equals(f6.get(), f5.get()), CPUINFO_TRUE);

    auto f7 = uset { cpuinfo_featureset_new(family_b) };
    auto f8 = uset {
        cpuinfo_featureset_featureset_intersect(f5.get(), f7.get())
    };
    EXPECT_FALSE(static_cast<bool>(f8));
}

TEST(Featureset, Subtract) {
    auto f1 = uset { cpuinfo_featureset_all(family_a) };

    auto f2 = uset { cpuinfo_featureset_new(family_a) };

    auto f3 = uset {
        cpuinfo_featureset_featureset_subtract(f1.get(), f2.get())
    };
    EXPECT_EQ(cpuinfo_featureset_equals(f3.get(), f1.get()), CPUINFO_TRUE);

    auto f4 = uset {
        cpuinfo_featureset_featureset_subtract(f2.get(), f1.get())
    };
    EXPECT_EQ(cpuinfo_featureset_equals(f4.get(), f2.get()), CPUINFO_TRUE);

    auto f5 = uset { cpuinfo_featureset_new(family_a) };
    f5->features().insert({ a, b });
    auto f6 = uset {
        cpuinfo_featureset_featureset_subtract(f1.get(), f5.get())
    };
    auto f7 = uset { cpuinfo_featureset_new(family_a) };
    f7->features().insert(c);
    EXPECT_EQ(cpuinfo_featureset_equals(f6.get(), f7.get()), CPUINFO_TRUE);

    auto f8 = uset { cpuinfo_featureset_new(family_b) };
    auto f9 = uset {
        cpuinfo_featureset_featureset_subtract(f1.get(), f8.get())
    };
    EXPECT_FALSE(static_cast<bool>(f9));
}

TEST(Featureset, Encode) {
    auto xml_to_string = [](char* str) {
        auto ret = std::string { str };
        cpuinfo_xml_free(str);
        return ret;
    };

    auto expected = static_cast<const char*>(nullptr);
    auto actual = std::string {};

    auto f1 = uset { cpuinfo_featureset_new(family_a) };
    actual = xml_to_string(cpuinfo_featureset_xml_get(f1.get()));
    expected =
            "<featureset>\n"
            "  <family>A</family>\n"
            "  <features/>\n"
            "</featureset>";
    EXPECT_EQ(expected, actual);

    cpuinfo_featureset_feature_add(f1.get(), c);
    actual = xml_to_string(cpuinfo_featureset_xml_get(f1.get()));
    expected =
            "<featureset>\n"
            "  <family>A</family>\n"
            "  <features>\n"
            "    <feature>c</feature>\n"
            "  </features>\n"
            "</featureset>";
    EXPECT_EQ(expected, actual);

    cpuinfo_featureset_feature_add(f1.get(), b);
    actual = xml_to_string(cpuinfo_featureset_xml_get(f1.get()));
    expected =
            "<featureset>\n"
            "  <family>A</family>\n"
            "  <features>\n"
            "    <feature>b</feature>\n"
            "    <feature>c</feature>\n"
            "  </features>\n"
            "</featureset>";
    EXPECT_EQ(expected, actual);

    cpuinfo_featureset_feature_add(f1.get(), a);
    actual = xml_to_string(cpuinfo_featureset_xml_get(f1.get()));
    expected =
            "<featureset>\n"
            "  <family>A</family>\n"
            "  <features>\n"
            "    <feature>d</feature>\n"
            "  </features>\n"
            "</featureset>";
    EXPECT_EQ(expected, actual);
}

static void setup() {
    family_a->name() = "A";
    family_b->name() = "B";

    a->name() = "a";
    a->family() = family_a;

    b->name() = "b";
    b->family() = family_a;

    c->name() = "c";
    c->family() = family_a;

    d->name() = "d";
    d->family() = family_a;
    d->features() = { a, b, c };

    x->name() = "x";
    x->family() = family_b;

    cpuinfo_family_t::all.push_back(ufam { family_a });
    cpuinfo_family_t::all.push_back(ufam { family_b });

    cpuinfo_feature_t::all.push_back(ufeat { a });
    cpuinfo_feature_t::all.push_back(ufeat { b });
    cpuinfo_feature_t::all.push_back(ufeat { c });
    cpuinfo_feature_t::all.push_back(ufeat { d });
    cpuinfo_feature_t::all.push_back(ufeat { x });
};

int main(int argc, char* argv[]) {
    try {
        setup();
        testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
    } catch (const std::exception& e) {
        std::cerr << e.what();
    }

    return 1;
}
