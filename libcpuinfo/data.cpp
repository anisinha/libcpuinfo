// SPDX-License-Identifier: LGPL-2.1-or-later
// Copyright 2023 Tim Wiederhake

#include "data.hpp"

#include "libcpuinfo.hpp"

#include <cstring>
#include <libxml/parser.h>
#include <libxml/relaxng.h>
#include <memory>

struct DeleteXML {
    void operator()(xmlDoc* ptr) {
        if (ptr != nullptr) {
            xmlFreeDoc(ptr);
        }
    }

    void operator()(xmlNode* ptr) {
        if (ptr != nullptr) {
            xmlFreeNode(ptr);
        }
    }

    void operator()(xmlRelaxNGParserCtxt* ptr) {
        if (ptr != nullptr) {
            xmlRelaxNGFreeParserCtxt(ptr);
        }
    }

    void operator()(xmlRelaxNGValidCtxt* ptr) {
        if (ptr != nullptr) {
            xmlRelaxNGFreeValidCtxt(ptr);
        }
    }

    void operator()(xmlRelaxNG* ptr) {
        if (ptr != nullptr) {
            xmlRelaxNGFree(ptr);
        }
    }
};

XmlRange::Iterator& XmlRange::Iterator::operator++() {
    m_ptr = xmlNextElementSibling(m_ptr);
    return *this;
}

XmlRange::Iterator XmlRange::Iterator::operator++(int) {
    XmlRange::Iterator tmp = *this;
    ++(*this);
    return tmp;
}

XmlRange::Iterator XmlRange::begin() {
    return Iterator { xmlFirstElementChild(m_xml) };
}

XmlRange::Iterator XmlRange::end() {
    return Iterator { nullptr };
}

std::string cpuinfo_data_node_name(xmlNode* xml) {
    return reinterpret_cast<const char*>(xml->name);
}

void cpuinfo_data_decode(xmlNode* xml, std::string& value) {
    auto string = xmlNodeGetContent(xml);
    if (string == nullptr) {
        return;
    }

    value = reinterpret_cast<const char*>(string);
    xmlFree(string);
}

void cpuinfo_data_decode(xmlNode* xml, int& value) {
    auto string = std::string {};
    cpuinfo_data_decode(xml, string);

    try {
        value = std::stoi(string, nullptr, 0);
    } catch (...) {
        throw std::invalid_argument { "not a valid int: \"" + string + "\"" };
    }
}

xmlNode* cpuinfo_data_create(xmlNode* parent, const std::string& name) {
    return xmlNewChild(
            parent,
            nullptr,
            reinterpret_cast<const xmlChar*>(name.c_str()),
            nullptr);
}

xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const std::string& value,
        const std::string& name) {
    return xmlNewChild(
            parent,
            nullptr,
            reinterpret_cast<const xmlChar*>(name.c_str()),
            reinterpret_cast<const xmlChar*>(value.c_str()));
}

xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const int& value,
        const std::string& name) {
    return cpuinfo_data_encode(parent, std::to_string(value), name);
}

void cpuinfo_data_load(
        const std::string& filename,
        std::function<void(xmlNode*)> func) {
    auto message = std::string {};

    auto callback = [](void* context, xmlErrorPtr error) {
        auto message = static_cast<std::string*>(context);

        if (!message->empty()) {
            message->append("\n");
        }

        if (error->file != nullptr) {
            message->append(error->file);
            message->append(":");

            if (error->line != 0) {
                message->append(std::to_string(error->line));
                message->append(":");

                if (error->int2 != 0) {
                    message->append(std::to_string(error->int2));
                    message->append(":");
                }
            }

            message->append(" ");
        }

        auto msg = std::string { error->message };
        while (!msg.empty() && (msg.back() == '\n' || msg.back() == '\r')) {
            msg.pop_back();
        }

        message->append(msg);
    };

    xmlResetLastError();
    xmlSetStructuredErrorFunc(&message, callback);

    auto rng = std::unique_ptr<xmlRelaxNGParserCtxt, DeleteXML> {
        xmlRelaxNGNewParserCtxt(CPUINFO_DATADIR "/schema.rng")
    };

    if (!rng) {
        throw std::runtime_error { "Failed to create schema parser" };
    }

    xmlRelaxNGSetParserStructuredErrors(rng.get(), callback, &message);

    auto schema = std::unique_ptr<xmlRelaxNG, DeleteXML> {
        xmlRelaxNGParse(rng.get())
    };

    if (!schema) {
        throw std::runtime_error { message };
    }

    auto ctx = std::unique_ptr<xmlRelaxNGValidCtxt, DeleteXML> {
        xmlRelaxNGNewValidCtxt(schema.get())
    };

    if (!ctx) {
        throw std::runtime_error { message };
    }

    auto doc = std::unique_ptr<xmlDoc, DeleteXML> {
        xmlParseFile(filename.c_str())
    };

    if (!doc) {
        throw std::runtime_error { message };
    }

    auto result = xmlRelaxNGValidateDoc(ctx.get(), doc.get());
    if (result != 0) {
        throw std::runtime_error { message };
    }

    auto root = xmlDocGetRootElement(doc.get());
    func(root);
}

std::string cpuinfo_data_save(std::function<xmlNode*(xmlNode*)> func) {
    std::string ret {};

    auto iowrite = [](void* context, const char* buffer, int len) {
        auto ret = static_cast<std::string*>(context);
        ret->append(buffer, len);
        return len;
    };

    auto ioclose = [](void*) {
        return 0;
    };

    auto buffer = xmlOutputBufferCreateIO(iowrite, ioclose, &ret, nullptr);

    auto doc = std::unique_ptr<xmlDoc, DeleteXML> {
        xmlNewDoc(reinterpret_cast<const xmlChar*>("1.0"))
    };

    auto root = xmlNewDocNode(
            doc.get(),
            nullptr,
            reinterpret_cast<const xmlChar*>("root"),
            nullptr);
    xmlDocSetRootElement(doc.get(), root);

    auto node = func(root);

    xmlNodeDumpOutput(buffer, doc.get(), node, 0, 1, nullptr);
    xmlOutputBufferClose(buffer);

    return ret;
}

char* cpuinfo_data_strdup(const std::string& buffer) {
    /* NOLINTNEXTLINE: manual memory management */
    auto ret = static_cast<char*>(std::malloc(buffer.size() + 1));
    if (ret == nullptr) {
        cpuinfo_error_set("Failed to duplicate string");
        return nullptr;
    }

    std::memcpy(ret, buffer.data(), buffer.size() + 1);
    return ret;
}
