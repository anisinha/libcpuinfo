// SPDX-License-Identifier: LGPL-2.1-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include "libcpuinfo.hpp"

#include <libcpuinfo/alias.h>

#include <memory>
#include <string>
#include <vector>

struct cpuinfo_alias_t {
public:
    [[nodiscard]] cpuinfo_alias_t() noexcept = default;

    [[nodiscard]] const std::string& domain() const noexcept {
        return m_domain;
    }

    [[nodiscard]] std::string& domain() noexcept {
        return m_domain;
    }

    [[nodiscard]] const std::string& name() const noexcept {
        return m_name;
    }

    [[nodiscard]] std::string& name() noexcept {
        return m_name;
    }

    [[nodiscard]] bool operator==(const cpuinfo_alias_t& rhs) const noexcept {
        return (m_domain == rhs.m_domain) && (m_name == rhs.m_name);
    }

private:
    std::string m_name {};
    std::string m_domain {};
};

typedef struct _xmlNode xmlNode;
void cpuinfo_data_decode(xmlNode* xml, cpuinfo_alias_t& value);
xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const cpuinfo_alias_t& value,
        const std::string& name = "alias");

/** Internal. */
template<typename T>
T* alias_find_canonical(
        const std::vector<std::unique_ptr<T>>& vec,
        const char* name) {
    for (const auto& obj : vec) {
        if (obj->name() == name) {
            return obj.get();
        }
    }

    return nullptr;
}

/** Internal. */
template<typename T>
T* alias_find_alias(
        const std::vector<std::unique_ptr<T>>& vec,
        const char* name,
        const char* domain) {
    auto name_match = static_cast<T*>(nullptr);
    auto alias_match = static_cast<T*>(nullptr);

    for (const auto& obj : vec) {
        if (obj->name() == name) {
            name_match = obj.get();
        }

        for (const auto& alias : obj->aliases()) {
            if (alias.name() != name) {
                /* wrong name, check next alias */
                continue;
            }

            if (!alias.domain().empty() && alias.domain() == domain) {
                /* right name and alias, return immediately */
                return obj.get();
            }

            if (alias_match == nullptr || alias.domain().empty()) {
                /* prefer aliases with empty domain */
                alias_match = obj.get();
            }
        }
    }

    if (name_match != nullptr) {
        return name_match;
    }

    if (alias_match != nullptr) {
        return alias_match;
    }

    return nullptr;
}

/**
 * Find an object in its pool.
 *
 * @param func Name of calling function. Used for error reporting.
 * @param vec object pool.
 * @param name object alias name.
 * @param domain object alias domain.
 * @return the object or nullptr on error.
 * @see alias.h
 */
template<typename T>
T* alias_find(
        std::string_view func,
        const std::vector<std::unique_ptr<T>>& vec,
        const char* name,
        const char* domain) {

    if (name == nullptr) {
        if (domain == nullptr) {
            return cpuinfo_error_set_func_null(func);
        }

        /* @domain is formatted as "domain:name" */
        auto string = std::string { domain };
        auto pos = string.find(':');
        if (pos == std::string::npos) {
            return alias_find(func, vec, domain, "");
        }

        auto sdomain = string.substr(0, pos);
        auto sname = string.substr(pos + 1);
        return alias_find(func, vec, sname.c_str(), sdomain.c_str());
    }

    auto match = static_cast<T*>(nullptr);

    if (domain == nullptr) {
        /* looking for canonical name */
        match = alias_find_canonical(vec, name);
    } else {
        /* looking for an alias name */
        match = alias_find_alias(vec, name, domain);
    }

    if (match == nullptr) {
        cpuinfo_error_set("No such entity");
    }

    return match;
}

/**
 * Find an object's name in a given domain.
 *
 * @param func Name of calling function. Used for error reporting.
 * @param obj object.
 * @param domain domain.
 * @return the object's name in the specified domain, its canonical name
 *     if the object is not aliased in that domain, or nullptr on error.
 */
template<typename T>
const char* alias_get(std::string_view func, T* obj, const char* domain) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(func);
    }

    if (domain != nullptr && domain[0] != '\0') {
        for (const auto& alias : obj->aliases()) {
            if (alias.domain() == domain) {
                return alias.name().c_str();
            }
        }
    }

    return obj->name().c_str();
}

/**
 * Count the number of alias names of an object.
 *
 * @param func Name of calling function. Used for error reporting.
 * @param obj object.
 * @returns the number of alias names of an object.
 */
template<typename T>
size_t alias_count(std::string_view func, T* obj) {
    if (obj == nullptr) {
        cpuinfo_error_set_func_null(func);
        return 0;
    }

    return obj->aliases().size();
}

/**
 * Return alias information for an object.
 *
 * @param func Name of calling function. Used for error reporting.
 * @param obj object.
 * @param index index into the object's list of alias names.
 * @returns object's `index`th alias name or `nullptr` on error.
 */
template<typename T>
cpuinfo_alias_t* alias_index(std::string_view func, T* obj, size_t index) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(func);
    }

    if (index >= obj->aliases().size()) {
        return cpuinfo_error_set_index_oob(func);
    }

    return &obj->aliases()[index];
}
