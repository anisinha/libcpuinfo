// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "cpuinfo.hpp"

#include <getopt.h>
#include <iostream>

static constexpr auto usage_string = std::string_view {
    "Usage: cpuinfo endianness [OPTIONS] [ENDIANNESS [KEY]]\n"
    "Query known endiannesses.\n"
    "\n"
    "Options:\n"
    "  -h, --help\n"
    "        Display this help and exit.\n"
    "\n"
    "Available commands:\n"
    "  endianness\n"
    "        Show all known endiannesses.\n"
    "  endianness ENDIANNESS\n"
    "        Show all known information for endianness ENDIANNESS.\n"
    "  endianness ENDIANNESS (name|description)\n"
    "        Show information for endianness.\n"
};

static int cmd_endianness_help(Output& output) {
    output << usage_string;
    return 0;
}

static int cmd_endianness_entry(Output& output, char* argv[]) {
    auto obj = std::unique_ptr<cpuinfo_endianness_t, Deleter> {
        cpuinfo_endianness_by_name(nullptr, argv[0])
    };
    if (!obj) {
        std::cerr << "Error: " << cpuinfo_error_get() << '\n';
        return 1;
    }

    auto element = std::string { argv[1] };
    if (element == "name") {
        auto name = cpuinfo_endianness_name_get(obj.get(), nullptr);
        output << Value { name };
    } else if (element == "description") {
        auto description = cpuinfo_endianness_description_get(obj.get());
        output << Value { description };
    } else {
        std::cerr << "Error: Unknown element '" << element << "'\n";
        return 1;
    }

    output << '\n';
    return 0;
}

static int cmd_endianness_show(Output& output, char* argv[]) {
    auto obj = std::unique_ptr<cpuinfo_endianness_t, Deleter> {
        cpuinfo_endianness_by_name(nullptr, argv[0])
    };
    if (!obj) {
        std::cerr << "Error: " << cpuinfo_error_get() << '\n';
        return 1;
    }

    auto map = Map { false };
    map.add_value("name", cpuinfo_endianness_name_get(obj.get(), nullptr));

    auto count_aliases = cpuinfo_endianness_alias_count(obj.get());
    if (count_aliases > 0) {
        auto list = map.add_list("aliases");
        for (size_t i = 0; i < count_aliases; ++i) {
            auto alias = std::unique_ptr<cpuinfo_alias_t, Deleter> {
                cpuinfo_endianness_alias_get(obj.get(), i)
            };

            auto string = std::string {};
            string += cpuinfo_alias_domain_get(alias.get());
            string += ':';
            string += cpuinfo_alias_name_get(alias.get());
            list->add_value(string);
        }
    }

    map.add_value("description", cpuinfo_endianness_description_get(obj.get()));

    output << map << '\n';
    return 0;
}

static int cmd_endianness_list(Output& output) {
    auto list = List {};

    auto size = cpuinfo_endianness_count();
    for (size_t i = 0; i < size; ++i) {
        auto obj = std::unique_ptr<cpuinfo_endianness_t, Deleter> {
            cpuinfo_endianness_by_index(i)
        };

        list.add_value(cpuinfo_endianness_name_get(obj.get(), nullptr));
    }

    output << list << '\n';
    return 0;
}

int cmd_endianness(Output& output, int argc, char* argv[]) {
    auto opt_short = "+:h";
    option opt_long[] = {
        { "help", no_argument, nullptr, 'h' },
        { nullptr, 0, nullptr, 0 }
    };

    auto arg = -1;
    auto idx = -1;

    // NOLINTNEXTLINE: getopt_long is not thread safe
    while ((arg = getopt_long(argc, argv, opt_short, opt_long, &idx)) != -1) {
        switch (arg) {
        case 'h':
            return cmd_endianness_help(output);

        case ':':
            std::cerr << "Error: Missing argument for option\n";
            return 1;

        default:
            std::cerr << "Error: Invalid option\n";
            return 1;
        }
    }

    switch (argc - optind) {
    case 0:
        return cmd_endianness_list(output);

    case 1:
        return cmd_endianness_show(output, argv + optind);

    case 2:
        return cmd_endianness_entry(output, argv + optind);

    default:
        break;
    }

    return cmd_endianness_help(output);
}
