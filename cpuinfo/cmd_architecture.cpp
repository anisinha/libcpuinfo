// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "cpuinfo.hpp"

#include <getopt.h>
#include <iostream>

static constexpr auto usage_string = std::string_view {
    "Usage: cpuinfo arch [OPTIONS] [ARCH [KEY]]\n"
    "Query known architectures.\n"
    "\n"
    "Options:\n"
    "  -h, --help\n"
    "        Display this help and exit.\n"
    "\n"
    "Available commands:\n"
    "  arch\n"
    "        Show all known architectures.\n"
    "  arch ARCH\n"
    "        Show all known information for architecture ARCH.\n"
    "  arch ARCH (name|description|family|endianness|wordsize)\n"
    "        Show information for architecture.\n"
};

static int cmd_architecture_help(Output& output) {
    output << usage_string;
    return 0;
}

static int cmd_architecture_entry(Output& output, char* argv[]) {
    auto obj = std::unique_ptr<cpuinfo_architecture_t, Deleter> {
        cpuinfo_architecture_by_name(nullptr, argv[0])
    };
    if (!obj) {
        std::cerr << "Error: " << cpuinfo_error_get() << '\n';
        return 1;
    }

    auto element = std::string { argv[1] };
    if (element == "name") {
        output << Value { cpuinfo_architecture_name_get(obj.get(), nullptr) };
    } else if (element == "description") {
        output << Value { cpuinfo_architecture_description_get(obj.get()) };
    } else if (element == "family") {
        auto family = std::unique_ptr<cpuinfo_family_t, Deleter> {
            cpuinfo_architecture_family_get(obj.get())
        };
        output << Value { cpuinfo_family_name_get(family.get(), nullptr) };
    } else if (element == "endianness") {
        auto endianness = std::unique_ptr<cpuinfo_endianness_t, Deleter> {
            cpuinfo_architecture_endianness_get(obj.get())
        };
        output << Value {
            cpuinfo_endianness_name_get(endianness.get(), nullptr)
        };
    } else if (element == "wordsize") {
        output << Value { cpuinfo_architecture_wordsize_get(obj.get()) };
    } else {
        std::cerr << "Error: Unknown element '" << element << "'\n";
        return 1;
    }

    output << '\n';
    return 0;
}

static int cmd_architecture_show(Output& output, char* argv[]) {
    auto obj = std::unique_ptr<cpuinfo_architecture_t, Deleter> {
        cpuinfo_architecture_by_name(nullptr, argv[0])
    };
    if (!obj) {
        std::cerr << "Error: " << cpuinfo_error_get() << '\n';
        return 1;
    }

    auto map = Map { false };
    map.add_value("name", cpuinfo_architecture_name_get(obj.get(), nullptr));

    auto count_aliases = cpuinfo_architecture_alias_count(obj.get());
    if (count_aliases != 0) {
        auto list = map.add_list("aliases");
        for (size_t i = 0; i < count_aliases; ++i) {
            auto alias = std::unique_ptr<cpuinfo_alias_t, Deleter> {
                cpuinfo_architecture_alias_get(obj.get(), i)
            };

            auto string = std::string {};
            string += cpuinfo_alias_domain_get(alias.get());
            string += ':';
            string += cpuinfo_alias_name_get(alias.get());
            list->add_value(string);
        }
    }

    map.add_value(
            "description",
            cpuinfo_architecture_description_get(obj.get()));

    auto family = std::unique_ptr<cpuinfo_family_t, Deleter> {
        cpuinfo_architecture_family_get(obj.get())
    };
    if (family) {
        map.add_value("family", cpuinfo_family_name_get(family.get(), nullptr));
    }

    auto endianness = std::unique_ptr<cpuinfo_endianness_t, Deleter> {
        cpuinfo_architecture_endianness_get(obj.get())
    };
    if (endianness) {
        map.add_value(
                "endianness",
                cpuinfo_endianness_name_get(endianness.get(), nullptr));
    }

    auto wordsize = cpuinfo_architecture_wordsize_get(obj.get());
    if (wordsize >= 0) {
        map.add_value("wordsize", wordsize);
    }

    output << map << '\n';
    return 0;
}

static int cmd_architecture_list(Output& output) {
    auto list = List {};

    auto size = cpuinfo_architecture_count();
    for (size_t i = 0; i < size; ++i) {
        auto obj = std::unique_ptr<cpuinfo_architecture_t, Deleter> {
            cpuinfo_architecture_by_index(i)
        };
        list.add_value(cpuinfo_architecture_name_get(obj.get(), nullptr));
    }

    output << list << '\n';
    return 0;
}

int cmd_architecture(Output& output, int argc, char* argv[]) {
    auto opt_short = "+:h";
    option opt_long[] = {
        { "help", no_argument, nullptr, 'h' },
        { nullptr, 0, nullptr, 0 }
    };

    auto arg = -1;
    auto idx = -1;

    // NOLINTNEXTLINE: getopt_long is not thread safe
    while ((arg = getopt_long(argc, argv, opt_short, opt_long, &idx)) != -1) {
        switch (arg) {
        case 'h':
            return cmd_architecture_help(output);

        case ':':
            std::cerr << "Error: Missing argument for option\n";
            return 1;

        default:
            std::cerr << "Error: Invalid option\n";
            return 1;
        }
    }

    switch (argc - optind) {
    case 0:
        return cmd_architecture_list(output);

    case 1:
        return cmd_architecture_show(output, argv + optind);

    case 2:
        return cmd_architecture_entry(output, argv + optind);

    default:
        break;
    }

    return cmd_architecture_help(output);
}
