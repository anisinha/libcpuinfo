// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "cpuinfo.hpp"

#include <getopt.h>
#include <iomanip>
#include <iostream>
#include <map>
#include <sstream>

#if __has_include(<unistd.h>)
#include <unistd.h>
#define STDOUT_IS_TTY (isatty(1))
#else
#define STDOUT_IS_TTY 0
#endif

std::string to_hex(size_t value, int width) {
    auto stream = std::stringstream {};
    stream
            << "0x"
            << std::setw(width)
            << std::setfill('0')
            << std::hex
            << value;
    return stream.str();
};

static int main_wrapped(int argc, char* argv[]) {
    using namespace std::literals;

    auto opt_short = "+:hv";
    option opt_long[] = {
        { "help", no_argument, nullptr, 'h' },
        { "version", no_argument, nullptr, 'v' },
        { "color", required_argument, nullptr, 1 },
        { "format", required_argument, nullptr, 2 },
        { nullptr, 0, nullptr, 0 }
    };

    auto arg = -1;
    auto idx = -1;
    auto short_help = false;
    auto short_version = false;
    auto out_format = Output::text;
    auto out_color = STDOUT_IS_TTY ? Output::colored : Output::monochrome;

    /* NOLINTNEXTLINE: getopt_long is not thread safe */
    while ((arg = getopt_long(argc, argv, opt_short, opt_long, &idx)) != -1) {
        switch (arg) {
        case 1:
            if ("off"sv == optarg) {
                out_color = Output::monochrome;
            } else if ("on"sv == optarg) {
                out_color = Output::colored;
            } else {
                std::cerr << "Error: '--color' must be 'on' or 'off'\n";
                return 1;
            }
            break;

        case 2:
            if ("text"sv == optarg) {
                out_format = Output::text;
            } else if ("json"sv == optarg) {
                out_format = Output::json;
            } else if ("xml"sv == optarg) {
                out_format = Output::xml;
            } else {
                std::cerr
                        << "Error: '--format' must be "
                        << "'text', 'json', or 'xml'\n";
                return 1;
            }
            break;

        case 'h':
            short_help = true;
            break;

        case 'v':
            short_version = true;
            break;

        case ':':
            std::cerr << "Error: Missing argument for option\n";
            return 1;

        default:
            std::cerr << "Error: Invalid option\n";
            return 1;
        }
    }

    auto output = Output { std::cout, out_format, out_color };

    if (short_version) {
        return cmd_version(output, argc, argv);
    }

    if (short_help || optind >= argc) {
        return cmd_help(output, argc, argv);
    }

    auto cmd = std::string { argv[optind] };
    optind += 1;

    auto cmds = std::map<std::string, int (*)(Output&, int, char*[])> {
        { "architecture", cmd_architecture },
        { "cpuid", cmd_cpuid },
        { "endianness", cmd_endianness },
        { "family", cmd_family },
        { "feature", cmd_feature },
        { "featureset", cmd_featureset },
        { "help", cmd_help },
        { "msr", cmd_msr },
        { "version", cmd_version },
    };

    auto it = cmds.find(cmd);
    if (it == cmds.end()) {
        std::cerr << "Error: Unknown command '" << cmd << "'\n";
        return 1;
    }

    return it->second(output, argc, argv);
}

int main(int argc, char* argv[]) {
    if (cpuinfo_init() != 0) {
        std::cerr
                << "Error: Failed to initialize libcpuinfo: "
                << cpuinfo_error_get()
                << '\n';
        return 1;
    }

    auto ret = main_wrapped(argc, argv);

    cpuinfo_quit();

    return ret;
}
