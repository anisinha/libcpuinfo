// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "output.hpp"

#include <libcpuinfo/libcpuinfo.h>
#include <memory>

class Deleter {
public:
    void operator()(cpuinfo_alias_t* obj) {
        cpuinfo_alias_free(obj);
    }

    void operator()(cpuinfo_endianness_t* obj) {
        cpuinfo_endianness_free(obj);
    }

    void operator()(cpuinfo_family_t* obj) {
        cpuinfo_family_free(obj);
    }

    void operator()(cpuinfo_architecture_t* obj) {
        cpuinfo_architecture_free(obj);
    }

    void operator()(cpuinfo_feature_t* obj) {
        cpuinfo_feature_free(obj);
    }

    void operator()(cpuinfo_featureset_t* obj) {
        cpuinfo_featureset_free(obj);
    }
};

std::string to_hex(size_t value, int width = 8);

int cmd_architecture(Output&, int argc, char* argv[]);

int cmd_cpuid(Output&, int argc, char* argv[]);

int cmd_endianness(Output&, int argc, char* argv[]);

int cmd_family(Output&, int argc, char* argv[]);

int cmd_feature(Output&, int argc, char* argv[]);

int cmd_featureset(Output&, int argc, char* argv[]);

int cmd_help(Output&, int argc, char* argv[]);

int cmd_msr(Output&, int argc, char* argv[]);

int cmd_version(Output&, int argc, char* argv[]);
