// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include <memory>
#include <ostream>
#include <vector>

class Value;
class List;
class Map;
class Table;

class Output {
public:
    class Implementation;

    enum Format {
        text,
        json,
        xml,
    };

    enum Color {
        monochrome,
        colored,
    };

    Output(std::ostream& stream, Format format, Color color);

    Output(const Output&) noexcept = delete;

    Output(Output&&) noexcept = delete;

    Output& operator=(const Output&) = delete;

    Output& operator=(Output&&) = delete;

    ~Output();

    [[nodiscard]] std::ostream& stream() const noexcept {
        return m_stream;
    }

    [[nodiscard]] const Format& format() const noexcept;

    [[nodiscard]] const Color& color() const noexcept;

    [[nodiscard]] const std::string& indent_string() const noexcept;

    [[nodiscard]] std::string& indent_string() noexcept;

    Output& operator<<(const Value& rhs) noexcept;

    Output& operator<<(const List& rhs) noexcept;

    Output& operator<<(const Map& rhs) noexcept;

    Output& operator<<(const Table& rhs) noexcept;

    template<typename T>
    Output& operator<<(const T& rhs) noexcept {
        m_stream << rhs;
        return *this;
    }

private:
    std::unique_ptr<Implementation> m_impl;
    std::ostream& m_stream;
};

class Printable {
public:
    Printable() noexcept = default;

    Printable(const Printable&) noexcept = default;

    Printable(Printable&&) noexcept = default;

    Printable& operator=(const Printable&) = default;

    Printable& operator=(Printable&&) = default;

    virtual ~Printable() = default;

    virtual void print(Output&) const = 0;

    [[nodiscard]] virtual const std::string& sort_key() const = 0;
};

class Value : public Printable {
public:
    explicit Value(bool value) :
        m_data { value ? "true" : "false" },
        m_literal { true } {
    }

    explicit Value(const char* value, bool literal = false) :
        m_data { value },
        m_literal { literal } {
    }

    explicit Value(std::string_view value, bool literal = false) :
        m_data { value },
        m_literal { literal } {
    }

    explicit Value(std::string value, bool literal = false) :
        m_data { std::move(value) },
        m_literal { literal } {
    }

    template<typename T>
    explicit Value(const T& value) :
        m_data { std::to_string(value) },
        m_literal { true } {
    }

    [[nodiscard]] const std::string& data() const noexcept {
        return m_data;
    }

    [[nodiscard]] const bool& literal() const noexcept {
        return m_literal;
    }

    void print(Output& output) const override {
        output << *this;
    }

    [[nodiscard]] const std::string& sort_key() const override {
        return m_data;
    }

private:
    std::string m_data;
    bool m_literal;
};

class List : public Printable {
public:
    typedef std::unique_ptr<Printable> element_type;

    explicit List(bool sort = false) :
        m_sort { sort } {
    }

    [[nodiscard]] const std::vector<element_type>& elements() const noexcept {
        return m_elements;
    }

    [[nodiscard]] const bool& sort() const noexcept {
        return m_sort;
    }

    template<typename T>
    Value* add_value(const T& value) {
        auto element = new Value { value };
        add(element);
        return element;
    }

    Map* add_map(const bool& sort = true);

    void print(Output& output) const override {
        output << *this;
    }

    [[nodiscard]] const std::string& sort_key() const override {
        return m_sort_key;
    }

private:
    std::vector<element_type> m_elements {};
    std::string m_sort_key {};
    bool m_sort;

    void add(Printable* printable) noexcept;
};

class Map : public Printable {
public:
    typedef std::pair<std::string, std::unique_ptr<Printable>> element_type;

    explicit Map(bool sort = true) :
        m_sort { sort } {
    }

    [[nodiscard]] const std::vector<element_type>& elements() const noexcept {
        return m_elements;
    }

    [[nodiscard]] const bool& sort() const noexcept {
        return m_sort;
    }

    template<typename T>
    Value* add_value(const std::string& key, const T& value) {
        auto element = new Value { value };
        add(key, element);
        return element;
    }

    List* add_list(const std::string& key, const bool& sort = true);

    Map* add_map(const std::string& key, const bool& sort = true);

    void print(Output& output) const override {
        output << *this;
    }

    [[nodiscard]] const std::string& sort_key() const override {
        return m_sort_key;
    }

private:
    std::vector<element_type> m_elements {};
    std::string m_sort_key {};
    bool m_sort;

    void add(const std::string& key, Printable* printable) noexcept;
};

class Table : public Printable {
public:
    typedef std::vector<std::pair<std::string, std::unique_ptr<Value>>>
            element_type;

    explicit Table(bool show_row_index = false) :
        m_show_row_index { show_row_index } {
    }

    template<typename T>
    Value* add_value(size_t row, const std::string& col, const T& value) {
        if (row == 0) {
            throw std::out_of_range { "Table is 1-indexed" };
        }

        auto element = new Value { value };
        add(row - 1, col, element);
        return element;
    }

    [[nodiscard]] const std::vector<element_type>& elements() const noexcept {
        return m_elements;
    }

    [[nodiscard]] const bool& show_row_index() const noexcept {
        return m_show_row_index;
    }

    void print(Output& output) const override {
        output << *this;
    }

    [[nodiscard]] const std::string& sort_key() const override {
        return m_sort_key;
    }

private:
    std::vector<element_type> m_elements {};
    std::string m_sort_key {};
    bool m_show_row_index;

    void add(size_t row, const std::string& col, Value* printable) noexcept;
};
