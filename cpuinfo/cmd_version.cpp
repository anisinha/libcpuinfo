// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "cpuinfo.hpp"

int cmd_version(Output& output, int /* argc */, char* /* argv */[]) {
    int major {};
    int minor {};
    int patch {};
    cpuinfo_version(&major, &minor, &patch);

    auto version = Map { false };
    version.add_value(
            "Copyright",
            "(C) 2023 Tim Wiederhake");
    version.add_value(
            "cpuinfo version",
            std::to_string(CPUINFO_VERSION_MAJOR) + //
                    "." + //
                    std::to_string(CPUINFO_VERSION_MINOR) + //
                    "." + //
                    std::to_string(CPUINFO_VERSION_PATCH));
    version.add_value(
            "libcpuinfo version",
            std::to_string(major) + //
                    "." + //
                    std::to_string(minor) + //
                    "." + //
                    std::to_string(patch));
    version.add_value(
            "Bugreports",
            "https://gitlab.com/twiederh/libcpuinfo");

    output << version << '\n';
    return 0;
}
