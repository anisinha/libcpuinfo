// SPDX-License-Identifier: LGPL-2.1-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include <libcpuinfo/common.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * Count number of known architectures.
 *
 * @return number of known architectures.
 */
extern size_t cpuinfo_architecture_count(void);

/**
 * Retrieve an architecture by its index in the pool.
 *
 * @param index index into the pool.
 * @return the `index`th entry in the pool or `NULL` on error.
 */
extern cpuinfo_architecture_t* cpuinfo_architecture_by_index(size_t index);

/**
 * Retrieve an architecture by its name.
 *
 * @param name name or alias of the object.
 * @param domain domain in which this name is used.
 * @return the requested object or `NULL` on error.
 * @see alias.h
 */
extern cpuinfo_architecture_t* cpuinfo_architecture_by_name(
        const char* name,
        const char* domain);

/**
 * Release a cpuinfo_architecture_t object.
 *
 * No-op if `obj` is NULL.
 *
 * @param obj object to release.
 */
extern void cpuinfo_architecture_free(cpuinfo_architecture_t* obj);

/**
 * Retrieve object's name.
 *
 * The returned string is internally allocated and must not be freed.
 *
 * @param obj object
 * @param domain alias domain or `NULL` for the canonical name.
 * @return the object's name or `NULL` on error.
 */
extern const char* cpuinfo_architecture_name_get(
        cpuinfo_architecture_t* obj,
        const char* domain);

/**
 * Count the number of alias names.
 *
 * @param obj object
 * @return the number of alias names.
 */
extern size_t cpuinfo_architecture_alias_count(cpuinfo_architecture_t* obj);

/**
 * Return alias information for an object.
 *
 * @param obj object
 * @param index index into the object's list of alias names.
 * @returns object's `index`th alias name or `NULL` on error.
 */
extern cpuinfo_alias_t* cpuinfo_architecture_alias_get(
        cpuinfo_architecture_t* obj,
        size_t index);

/**
 * Return object's description.
 *
 * The returned string is internally allocated and must not be freed.
 *
 * @param obj object
 * @return object's description or `NULL` on error.
 */
extern const char* cpuinfo_architecture_description_get(
        cpuinfo_architecture_t* obj);

/**
 * Return object's architecture family.
 *
 * @param obj object
 * @return object's architecture family or `NULL` on error.
 */
extern cpuinfo_family_t* cpuinfo_architecture_family_get(
        cpuinfo_architecture_t* obj);

/**
 * Return object's endianness.
 *
 * @param obj object
 * @return object's endianness or `NULL` on error.
 */
extern cpuinfo_endianness_t* cpuinfo_architecture_endianness_get(
        cpuinfo_architecture_t* obj);

/**
 * Return object's word size.
 *
 * @param obj object
 * @return object's word size or a negative value on error.
 */
extern int cpuinfo_architecture_wordsize_get(cpuinfo_architecture_t* obj);

/**
 * Return an object's xml representation.
 *
 * The returned string must be released by the application
 * using `cpuinfo_xml_free()`.
 *
 * @param obj object.
 * @return object's xml representation or `NULL` on error.
 */
extern char* cpuinfo_architecture_xml_get(cpuinfo_architecture_t* obj);

/**
 * Checks two architecture objects for equality.
 *
 * @param lhs first object.
 * @param rhs second object.
 * @return `CPUINFO_TRUE` if both objects are equal, `CPUINFO_FALSE` otherwise,
 *     and a negative value on error.
 */
extern int cpuinfo_architecture_equals(
        cpuinfo_architecture_t* lhs,
        cpuinfo_architecture_t* rhs);

#ifdef __cplusplus
}
#endif /* __cplusplus */
