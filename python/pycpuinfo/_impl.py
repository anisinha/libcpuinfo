from ctypes import (
    CFUNCTYPE,
    POINTER,
    byref,
    c_char_p,
    c_int,
    c_size_t,
    c_uint32,
    c_void_p,
    cast,
    cdll,
)

CFUNCTYPE
byref


def xml_to_string(ptr):
    if not ptr:
        return None
    string = ptr_to_string(cast(ptr, c_char_p).value)
    xml_free(ptr)
    return string


def ptr_to_string(ptr):
    if ptr is None:
        return None
    return ptr.decode("UTF-8")


def string_to_ptr(string):
    if string is None:
        return None
    return string.encode("UTF-8")


def raise_if_negative(value):
    if value >= 0:
        return
    raise RuntimeError(ptr_to_string(error_get()))


def wrap(name, argtypes, restype):
    function = getattr(handle, "cpuinfo_" + name)
    function.argtypes = argtypes
    function.restype = restype
    return function


def _find_library():
    path = "libcpuinfo.so"
    import os
    with os.popen("pkg-config --libs-only-L libcpuinfo") as p:
        output = p.read().strip()
    if output:
        path = os.path.join(output[2:], path)
    return cdll.LoadLibrary(path)


handle = _find_library()

alias_domain_get = wrap("alias_domain_get", [c_void_p], c_char_p)
alias_equals = wrap("alias_equals", [c_void_p, c_void_p], c_int)
alias_free = wrap("alias_free", [c_void_p], None)
alias_name_get = wrap("alias_name_get", [c_void_p], c_char_p)
alias_xml_get = wrap("alias_xml_get", [c_void_p], c_void_p)
architecture_alias_count = wrap("architecture_alias_count", [c_void_p], c_size_t)
architecture_alias_get = wrap("architecture_alias_get", [c_void_p, c_size_t], c_void_p)
architecture_by_index = wrap("architecture_by_index", [c_size_t], c_void_p)
architecture_by_name = wrap("architecture_by_name", [c_char_p, c_char_p], c_void_p)
architecture_count = wrap("architecture_count", [], c_size_t)
architecture_description_get = wrap("architecture_description_get", [c_void_p], c_char_p)
architecture_endianness_get = wrap("architecture_endianness_get", [c_void_p], c_void_p)
architecture_equals = wrap("architecture_equals", [c_void_p, c_void_p], c_int)
architecture_family_get = wrap("architecture_family_get", [c_void_p], c_void_p)
architecture_free = wrap("architecture_free", [c_void_p], None)
architecture_name_get = wrap("architecture_name_get", [c_void_p, c_char_p], c_char_p)
architecture_wordsize_get = wrap("architecture_wordsize_get", [c_void_p], c_int)
architecture_xml_get = wrap("architecture_xml_get", [c_void_p], c_void_p)
endianness_alias_count = wrap("endianness_alias_count", [c_void_p], c_size_t)
endianness_alias_get = wrap("endianness_alias_get", [c_void_p, c_size_t], c_void_p)
endianness_by_by_name = wrap("endianness_by_name", [c_char_p, c_char_p], c_void_p)
endianness_by_index = wrap("endianness_by_index", [c_size_t], c_void_p)
endianness_count = wrap("endianness_count", [], c_size_t)
endianness_description_get = wrap("endianness_description_get", [c_void_p], c_char_p)
endianness_equals = wrap("endianness_equals", [c_void_p, c_void_p], c_int)
endianness_free = wrap("endianness_free", [c_void_p], None)
endianness_name_get = wrap("endianness_name_get", [c_void_p, c_char_p], c_char_p)
endianness_xml_get = wrap("endianness_xml_get", [c_void_p], c_void_p)
error_clear = wrap("error_clear", [], None)
error_get = wrap("error_get", [], c_char_p)
family_alias_count = wrap("family_alias_count", [c_void_p], c_size_t)
family_alias_get = wrap("family_alias_get", [c_void_p, c_size_t], c_void_p)
family_by_index = wrap("family_by_index", [c_size_t], c_void_p)
family_by_name = wrap("family_by_name", [c_char_p, c_char_p], c_void_p)
family_count = wrap("family_count", [], c_size_t)
family_description_get = wrap("family_description_get", [c_void_p], c_char_p)
family_equals = wrap("family_equals", [c_void_p, c_void_p], c_int)
family_free = wrap("family_free", [c_void_p], None)
family_name_get = wrap("family_name_get", [c_void_p, c_char_p], c_char_p)
family_xml_get = wrap("family_xml_get", [c_void_p], c_void_p)
feature_alias_count = wrap("feature_alias_count", [c_void_p], c_size_t)
feature_alias_get = wrap("feature_alias_get", [c_void_p, c_size_t], c_void_p)
feature_by_index = wrap("feature_by_index", [c_size_t], c_void_p)
feature_by_name = wrap("feature_by_name", [c_char_p, c_char_p], c_void_p)
feature_count = wrap("feature_count", [], c_size_t)
feature_description_get = wrap("feature_description_get", [c_void_p], c_char_p)
feature_equals = wrap("alias_equals", [c_void_p, c_void_p], c_int)
feature_family_get = wrap("feature_family_get", [c_void_p], c_void_p)
feature_features_count = wrap("feature_features_count", [c_void_p], c_size_t)
feature_features_get = wrap("feature_features_get", [c_void_p, c_size_t], c_void_p)
feature_free = wrap("feature_free", [c_void_p], None)
feature_hostsupport_get = wrap("feature_hostsupport_get", [c_void_p], c_int)
feature_name_get = wrap("feature_name_get", [c_void_p, c_char_p], c_char_p)
feature_x86_extra_cpuid_get = wrap("feature_x86_extra_cpuid_get", [c_void_p, POINTER(c_uint32), POINTER(c_uint32), POINTER(c_uint32), POINTER(c_uint32), POINTER(c_uint32), POINTER(c_uint32)], c_int)
feature_x86_extra_msr_get = wrap("feature_x86_extra_msr_get", [c_void_p, POINTER(c_uint32), POINTER(c_uint32), POINTER(c_uint32)], c_int)
feature_xml_get = wrap("feature_xml_get", [c_void_p], c_void_p)
featureset_all = wrap("featureset_all", [c_void_p], c_void_p)
featureset_clone = wrap("featureset_clone", [c_void_p], c_void_p)
featureset_equals = wrap("featureset_equals", [c_void_p, c_void_p], c_int)
featureset_family_get = wrap("featureset_family_get", [c_void_p], c_void_p)
featureset_feature_add = wrap("featureset_feature_add", [c_void_p, c_void_p], c_int)
featureset_feature_clear = wrap("featureset_feature_clear", [c_void_p], None)
featureset_feature_contains = wrap("featureset_feature_contains", [c_void_p, c_void_p], c_int)
featureset_feature_count = wrap("featureset_feature_count", [c_void_p], c_size_t)
featureset_feature_get = wrap("featureset_feature_get", [c_void_p, c_size_t], c_void_p)
featureset_feature_remove = wrap("featureset_feature_remove", [c_void_p, c_void_p], c_int)
featureset_featureset_intersect = wrap("featureset_featureset_intersect", [c_void_p, c_void_p], c_void_p)
featureset_featureset_subtract = wrap("featureset_featureset_subtract", [c_void_p, c_void_p], c_void_p)
featureset_featureset_union = wrap("featureset_featureset_union", [c_void_p, c_void_p], c_void_p)
featureset_free = wrap("featureset_free", [c_void_p], None)
featureset_host = wrap("featureset_host", [], c_void_p)
featureset_new = wrap("featureset_new", [c_void_p], c_void_p)
featureset_xml_get = wrap("featureset_xml_get", [c_void_p], c_void_p)
featureset_xml_set = wrap("featureset_xml_set", [c_char_p], c_void_p)
init = wrap("init", [], c_int)
quit = wrap("quit", [], None)
version = wrap("version", [c_void_p, c_void_p, c_void_p], None)
x86_cpuid = wrap("x86_cpuid", [c_uint32, c_uint32, POINTER(c_uint32), POINTER(c_uint32), POINTER(c_uint32), POINTER(c_uint32)], c_int)
x86_cpuid_foreach = wrap("x86_cpuid_foreach", [c_void_p, c_void_p], c_int)
x86_msr_read = wrap("x86_msr_read", [c_uint32, POINTER(c_uint32), POINTER(c_uint32)], c_int)
x86_msr_write = wrap("x86_msr_write", [c_uint32, c_uint32, c_uint32], c_int)
xml_free = wrap("xml_free", [c_void_p], None)
