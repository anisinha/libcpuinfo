from . import _impl
from . import x86

if _impl.init() != 0:
    raise ImportError(_impl.ptr_to_string(_impl.error_get()))


class Alias:
    def __init__(self, handle):
        self._handle = handle

    def __del__(self):
        _impl.alias_free(self._handle)

    def __str__(self) -> str:
        return f"{self.domain()}:{self.name()}"

    def __eq__(self, other: "Alias") -> bool:
        if not isinstance(other, Alias):
            raise ValueError
        ret = _impl.alias_equals(self._handle, other._handle)
        _impl.raise_if_negative(ret)
        return ret == 1

    def __ne__(self, other: "Alias") -> bool:
        return not (self == other)

    def __hash__(self) -> int:
        return int(self._handle)

    def name(self) -> str:
        ret = _impl.alias_name_get(self._handle)
        return _impl.ptr_to_string(ret)

    def domain(self) -> str:
        ret = _impl.alias_domain_get(self._handle)
        return _impl.ptr_to_string(ret)

    def xml(self) -> str:
        ret = _impl.alias_xml_get(self._handle)
        return _impl.xml_to_string(ret)


class Endianness:
    @classmethod
    def find(cls, name: str, domain: str = None) -> "Endianness":
        name = _impl.string_to_ptr(name)
        domain = _impl.string_to_ptr(domain)
        handle = _impl.endianness_by_by_name(name, domain)
        return None if handle is None else Endianness(handle)

    def __init__(self, handle):
        self._handle = handle

    def __del__(self):
        _impl.endianness_free(self._handle)

    def __str__(self) -> str:
        return self.name()

    def __eq__(self, other: "Endianness") -> bool:
        if not isinstance(other, Endianness):
            raise ValueError
        ret = _impl.endianness_equals(self._handle, other._handle)
        _impl.raise_if_negative(ret)
        return ret == 1

    def __ne__(self, other: "Endianness") -> bool:
        return not (self == other)

    def __hash__(self) -> int:
        return int(self._handle)

    def name(self, domain: str = None) -> str:
        domain = _impl.string_to_ptr(domain)
        ret = _impl.endianness_name_get(self._handle, domain)
        return _impl.ptr_to_string(ret)

    def description(self) -> str:
        ret = _impl.endianness_description_get(self._handle)
        return _impl.ptr_to_string(ret)

    def aliases(self) -> [Alias]:
        for i in range(0, _impl.endianness_alias_count(self._handle)):
            yield Alias(_impl.endianness_alias_get(self._handle, i))


class Family:
    @classmethod
    def find(cls, name, domain: str = None) -> "Family":
        name = _impl.string_to_ptr(name)
        domain = _impl.string_to_ptr(domain)
        handle = _impl.family_by_name(name, domain)
        return None if handle is None else Family(handle)

    def __init__(self, handle):
        self._handle = handle

    def __del__(self):
        _impl.family_free(self._handle)

    def __str__(self) -> str:
        return self.name()

    def __eq__(self, other: "Family") -> bool:
        if not isinstance(other, Family):
            raise ValueError
        ret = _impl.family_equals(self._handle, other._handle)
        _impl.raise_if_negative(ret)
        return ret == 1

    def __ne__(self, other: "Family") -> bool:
        return not (self == other)

    def __hash__(self) -> int:
        return int(self._handle)

    def name(self, domain: str = None) -> str:
        domain = _impl.string_to_ptr(domain)
        ret = _impl.family_name_get(self._handle, domain)
        return _impl.ptr_to_string(ret)

    def description(self) -> str:
        ret = _impl.family_description_get(self._handle)
        return _impl.ptr_to_string(ret)

    def aliases(self) -> [Alias]:
        for i in range(0, _impl.family_alias_count(self._handle)):
            yield Alias(_impl.family_alias_get(self._handle, i))


class Architecture:
    @classmethod
    def find(cls, name, domain: str = None) -> "Architecture":
        name = _impl.string_to_ptr(name)
        domain = _impl.string_to_ptr(domain)
        handle = _impl.architecture_by_name(name, domain)
        return None if handle is None else Architecture(handle)

    def __init__(self, handle):
        self._handle = handle

    def __del__(self):
        _impl.architecture_free(self._handle)

    def __str__(self) -> str:
        return self.name()

    def __eq__(self, other: "Architecture") -> bool:
        if not isinstance(other, Architecture):
            raise ValueError
        ret = _impl.architecture_equals(self._handle, other._handle)
        _impl.raise_if_negative(ret)
        return ret == 1

    def __ne__(self, other: "Architecture") -> bool:
        return not (self == other)

    def __hash__(self) -> int:
        return int(self._handle)

    def name(self, domain: str = None) -> str:
        domain = _impl.string_to_ptr(domain)
        ret = _impl.architecture_name_get(self._handle, domain)
        return _impl.ptr_to_string(ret)

    def description(self) -> str:
        ret = _impl.architecture_description_get(self._handle)
        return _impl.ptr_to_string(ret)

    def aliases(self) -> [Alias]:
        for i in range(0, _impl.architecture_alias_count(self._handle)):
            yield Alias(_impl.architecture_alias_get(self._handle, i))

    def family(self) -> Family:
        ret = _impl.architecture_family_get(self._handle)
        return None if ret is None else Family(ret)

    def endianness(self) -> Endianness:
        ret = _impl.architecture_endianness_get(self._handle)
        return None if ret is None else Endianness(ret)

    def wordsize(self) -> int:
        return _impl.architecture_wordsize_get(self._handle)


class Feature:
    @classmethod
    def find(cls, name, domain: str = None) -> "Feature":
        name = _impl.string_to_ptr(name)
        domain = _impl.string_to_ptr(domain)
        handle = _impl.feature_by_name(name, domain)
        return None if handle is None else Feature(handle)

    def __init__(self, handle):
        self._handle = handle

    def __del__(self):
        _impl.feature_free(self._handle)

    def __str__(self) -> str:
        return self.name()

    def __eq__(self, other: "Feature") -> bool:
        if not isinstance(other, Feature):
            raise ValueError
        ret = _impl.feature_equals(self._handle, other._handle)
        _impl.raise_if_negative(ret)
        return ret == 1

    def __ne__(self, other: "Feature") -> bool:
        return not (self == other)

    def __hash__(self) -> int:
        return int(self._handle)

    def name(self, domain: str = None) -> str:
        domain = _impl.string_to_ptr(domain)
        ret = _impl.feature_name_get(self._handle, domain)
        return _impl.ptr_to_string(ret)

    def description(self) -> str:
        ret = _impl.feature_description_get(self._handle)
        return _impl.ptr_to_string(ret)

    def aliases(self) -> [Alias]:
        for i in range(0, _impl.feature_alias_count(self._handle)):
            yield Alias(_impl.feature_alias_get(self._handle, i))

    def extra_x86_cpuid(self) -> (int, int, int, int, int, int):
        return x86._feature_x86_extra_cpuid_get(self._handle)

    def extra_x86_msr(self) -> (int, int, int):
        return x86._feature_x86_extra_msr_get(self._handle)

    def family(self) -> Family:
        ret = _impl.feature_family_get(self._handle)
        return None if ret is None else Family(ret)

    def features(self) -> ["Feature"]:
        for i in range(0, _impl.feature_features_count(self._handle)):
            yield Feature(_impl.feature_features_get(self._handle, i))

    def hostsupport(self) -> bool:
        ret = _impl.feature_hostsupport_get(self._handle)
        _impl.raise_if_negative(ret)
        return ret == 1


class Featureset:
    @classmethod
    def empty(cls, family: Family = None) -> "Featureset":
        if family is None:
            family = Family.find("host", "")
        ret = _impl.featureset_new(family._handle)
        return None if ret is None else Featureset(ret)

    @classmethod
    def all(cls, family: Family = None) -> "Featureset":
        if family is None:
            family = Family.find("host", "")
        ret = _impl.featureset_all(family._handle)
        return None if ret is None else Featureset(ret)

    @classmethod
    def host(cls) -> "Featureset":
        ret = _impl.featureset_host()
        return None if ret is None else Featureset(ret)

    def __init__(self, handle):
        self._handle = handle

    def __del__(self):
        _impl.featureset_free(self._handle)

    def __eq__(self, other: "Featureset") -> bool:
        if not isinstance(other, Featureset):
            raise ValueError
        ret = _impl.featureset_equals(self._handle, other._handle)
        _impl.raise_if_negative(ret)
        return ret == 1

    def __ne__(self, other: "Featureset") -> bool:
        return not (self == other)

    def __hash__(self) -> int:
        return int(self._handle)

    def __len__(self) -> int:
        ret = _impl.featureset_feature_count(self._handle)
        return ret

    def __iter__(self):
        return self.features()

    def family(self) -> Family:
        ret = _impl.featureset_family_get(self._handle)
        return None if ret is None else Family(ret)

    def features(self) -> [Feature]:
        for i in range(0, _impl.featureset_feature_count(self._handle)):
            yield Feature(_impl.featureset_feature_get(self._handle, i))

    def add(self, feature: Feature) -> None:
        _impl.featureset_feature_add(self._handle, feature._handle)

    def clear(self) -> None:
        _impl.featureset_feature_clear(self._handle)

    def copy(self) -> "Featureset":
        ret = _impl.featureset_clone(self._handle)
        return None if ret is None else Featureset(ret)

    def difference(self, other: "Featureset") -> "Featureset":
        ret = _impl.featureset_featureset_subtract(self._handle, other._handle)
        return None if ret is None else Featureset(ret)

    def discard(self, feature: Feature) -> None:
        _impl.featureset_feature_remove(self._handle, feature._handle)

    def intersection(self, other: "Featureset") -> "Featureset":
        ret = _impl.featureset_featureset_intersect(
            self._handle,
            other._handle)
        return None if ret is None else Featureset(ret)

    def isdisjoint(self, other: "Featureset") -> bool:
        return len(self.intersection(other)) == 0

    def issubset(self, other: "Featureset") -> bool:
        return self.intersection(other) == self

    def issuperset(self, other: "Featureset") -> bool:
        return self.intersection(other) == other

    def remove(self, feature: Feature) -> None:
        ret = _impl.featureset_feature_remove(self._handle, feature._handle)
        _impl.raise_if_negative(ret)
        if ret != 1:
            raise KeyError(feature)

    def union(self, other: "Featureset") -> "Featureset":
        ret = _impl.featureset_featureset_union(self._handle, other._handle)
        return None if ret is None else Featureset(ret)

    def update(self, other: "Featureset") -> None:
        ret = self.union(other)
        self._handle, ret._handle = ret._handle, self._handle


def endiannesses():
    for i in range(0, _impl.endianness_count()):
        yield Endianness(_impl.endianness_by_index(i))


def families():
    for i in range(0, _impl.family_count()):
        yield Family(_impl.family_by_index(i))


def architectures():
    for i in range(0, _impl.architecture_count()):
        yield Architecture(_impl.architecture_by_index(i))


def features():
    for i in range(0, _impl.feature_count()):
        yield Feature(_impl.feature_by_index(i))


def version():
    ver_major = _impl.c_int()
    ver_minor = _impl.c_int()
    ver_patch = _impl.c_int()
    _impl.version(
        _impl.byref(ver_major),
        _impl.byref(ver_minor),
        _impl.byref(ver_patch))
    return (ver_major.value, ver_minor.value, ver_patch.value)
