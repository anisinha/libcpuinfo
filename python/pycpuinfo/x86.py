from . import _impl

CPUINFO_X86_CPUID_LEAF_BASIC = 0x00000000
CPUINFO_X86_CPUID_LEAF_EXTENDED = 0x80000000
CPUINFO_X86_CPUID_ECX_NONE = 0x656E6F4E


def cpuid(eax_in: int, ecx_in: int = 0) -> (int, int, int, int):
    eax = _impl.c_uint32()
    ebx = _impl.c_uint32()
    ecx = _impl.c_uint32()
    edx = _impl.c_uint32()
    ret = _impl.x86_cpuid(
        eax_in,
        ecx_in,
        _impl.byref(eax),
        _impl.byref(ebx),
        _impl.byref(ecx),
        _impl.byref(edx))
    if ret != 0:
        raise RuntimeError(_impl.ptr_to_string(_impl.error_get()))
    return (eax.value, ebx.value, ecx.value, edx.value)


def cpuids() -> dict:
    entries = dict()

    @_impl.CFUNCTYPE(
        _impl.c_int,
        _impl.c_uint32,
        _impl.c_uint32,
        _impl.c_uint32,
        _impl.c_uint32,
        _impl.c_uint32,
        _impl.c_uint32,
        _impl.c_void_p)
    def callback(in_eax, in_ecx, eax, ebx, ecx, edx, _):
        entries[(in_eax, in_ecx)] = (eax, ebx, ecx, edx)
        return 0

    ret = _impl.x86_cpuid_foreach(callback, None)
    if ret != 0:
        raise RuntimeError(_impl.ptr_to_string(_impl.error_get()))
    return entries


def _feature_x86_extra_cpuid_get(handle) -> (int, int, int, int, int, int):
    eax_in = _impl.c_uint32()
    ecx_in = _impl.c_uint32()
    eax = _impl.c_uint32()
    ebx = _impl.c_uint32()
    ecx = _impl.c_uint32()
    edx = _impl.c_uint32()
    ret = _impl.feature_x86_extra_cpuid_get(
        handle,
        _impl.byref(eax_in),
        _impl.byref(ecx_in),
        _impl.byref(eax),
        _impl.byref(ebx),
        _impl.byref(ecx),
        _impl.byref(edx))
    _impl.raise_if_negative(ret)
    if ret != 0:
        return None
    return (
        eax_in.value,
        ecx_in.value,
        eax.value,
        ebx.value,
        ecx.value,
        edx.value)


def _feature_x86_extra_msr_get(handle) -> (int, int, int):
    index = _impl.c_uint32()
    eax = _impl.c_uint32()
    edx = _impl.c_uint32()
    ret = _impl.feature_x86_extra_msr_get(
        handle,
        _impl.byref(index),
        _impl.byref(eax),
        _impl.byref(edx))
    _impl.raise_if_negative(ret)
    if ret != 0:
        return None
    return (index.value, eax.value, edx.value)


def msr_read(index: int) -> (int, int):
    eax = _impl.c_uint32()
    edx = _impl.c_uint32()
    ret = _impl.x86_msr_read(index, _impl.byref(eax), _impl.byref(edx))
    _impl.raise_if_negative(ret)
    return (eax.value, edx.value)


def msr_write(index: int, eax: int, ecx: int) -> None:
    ret = _impl.x86_msr_write(index, eax, ecx)
    _impl.raise_if_negative(ret)
